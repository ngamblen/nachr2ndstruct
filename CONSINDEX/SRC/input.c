/*************************************************************************
 *                               INPUT.C                                 *
 *************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re (lenov@pasteur.fr)
      Receptors and Cognition 
      Pasteur Institute
      75724 Paris cedex 15, France
*/
/*>>>>>>>>>>>>>>>>>>>>>>PREPROCESSOR INFORMATIONS<<<<<<<<<<<<<<<<<<<<<<<<*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "input.h"
#include "matrix.h"

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/****************************
 * input alignment function *
 ****************************/

void InputAlign(char *alignname)
{
    int  seq=0,pos=0;		 /*counters*/
    char line[100],seqline[100];
    FILE *inputal;		 /*ptr on alignment file*/
    
    /*----------------*
     | init variables |
     *----------------*/
    
    memset(alignment[0].name,'\0',NUMBER*NAME);
    memset(alignment[0].sequence,'\0',NUMBER*SIZE);
    actualnum=0,actualsize=0;
    
    inputal=fopen(alignname,"r"); /*open the alignment file*/
    if (inputal==NULL)
    {
	fprintf(stderr,"no alignment file found\n");
	usage();
	exit(1);
    }
    
    /*---------------------*
     | Read the first line |
     *---------------------*/
    
    fgets(line,sizeof(line),inputal);   /*readthrough the comment line*/
    
    /*-------------------------*
     | read the remaining file |
     *-------------------------*/
    
    while(1)
    {
	fgets(line,sizeof(line),inputal);
	if (feof(inputal))
	    break;
	if (line[0] != '\n' && line[0] != '\0' && line[0] != ' ') /*beginning of a new block*/
	{
	    sscanf(line,"%s %s",alignment[seq].name,seqline);
	    strcat(alignment[seq].sequence,seqline);
	    seq++;					    /*next sequence*/
	}
	if (line[0]=='\n' || line[0]=='\0' || line[0]==' ')
	    seq=0;                             /*at the end of the block, return to the first sequence*/
    }
    
    fclose(inputal);                           /*close the alignment file*/
    seq=0;
    while (alignment[seq].sequence[0] !='\0'){ /*computes the actual number of sequences*/
	actualnum++,seq++;                     /*seq is variable, position is 0*/
    }
    pos=0;
    while (alignment[0].sequence[pos]!='\0'){  /*computes the actual length of alignment*/
	actualsize++,pos++;                    /*pos is variable, seq is 0*/
    }
}

/*************************
 * input matrix function *
 *************************/

void InputMatrix(char *matname)
{
    int i,j;			/*loop counters*/	
    int matcode=0;		/*code the matrix identity (for the switch)*/
    char *tmp_ptr;		/*pointer to screen strings*/
    
    /*----------------*
     | init variables |
     *----------------*/
    
    memset(usedmatrix.namemat,'\0',22);
    memset(usedmatrix.residue,'\0',RESIDUES);
    memset(usedmatrix.value,0,RESIDUES*RESIDUES);
    
    
    if (!strncmp(matname,"default",5))
	matcode=1;
    if (!strncmp(matname,"nucident",5))
	matcode=2;
    if (!strncmp(matname,"pepident",5))
	matcode=3;
    if (!strncmp(matname,"gcggap",5))
	matcode=4;
    if (!strncmp(matname,"pam250",5))
	matcode=5;
    if(matcode==0)
    {
	fprintf(stderr,"Unknown matrix: %s\n",matname);
	exit(1);
    }
    
    switch (matcode)
    {
	case 1:
	    strcpy(usedmatrix.namemat,gcggap.namemat);
	    strcpy(usedmatrix.residue,gcggap.residue);
	    for (i=0;i<RESIDUES;i++)
		for (j=0;j<RESIDUES;j++)
		    usedmatrix.value[i][j]=gcggap.value[i][j];
	    break;
	case 2:
	    strcpy(usedmatrix.namemat,nucident.namemat);
	    strcpy(usedmatrix.residue,nucident.residue);
	    for (i=0;i<RESIDUES;i++)
		for (j=0;j<RESIDUES;j++)
		    usedmatrix.value[i][j]=nucident.value[i][j];
	    break;
	case 3:
	    strcpy(usedmatrix.namemat,pepident.namemat);
	    strcpy(usedmatrix.residue,pepident.residue);
	    for (i=0;i<RESIDUES;i++)
		for (j=0;j<RESIDUES;j++)
		    usedmatrix.value[i][j]=pepident.value[i][j];
	    break;
	case 4:
	    strcpy(usedmatrix.namemat,gcggap.namemat);
	    strcpy(usedmatrix.residue,gcggap.residue);
	    for (i=0;i<RESIDUES;i++)
		for (j=0;j<RESIDUES;j++)
		    usedmatrix.value[i][j]=gcggap.value[i][j];
	    break;
	case 5:
	    strcpy(usedmatrix.namemat,pam250.namemat);
	    strcpy(usedmatrix.residue,pam250.residue);
	    for (i=0;i<RESIDUES;i++)
		for (j=0;j<RESIDUES;j++)
		    usedmatrix.value[i][j]=pam250.value[i][j];
	    break;
	default:
	    break;
    }
    tmp_ptr=usedmatrix.residue;
    while (*tmp_ptr!='\0')
    {	
	tmp_ptr++;
	actualres++;
    }
}
