/*************************************************************************
 *                             ConsIndex v1.11                           *
 *  Author: Nicolas Le Nov�re 1998-2004	                                 *
 *  Usage:  consindex -A[alignment] -M[matrix] -O[outfile] -V            *
 *  File formats:                                                        *
 *     Infile: Clustal-like format. Each line constains the sequence     *
 *             label and a line of nucleotides max. An empty line form   *
 *             the separation between the block of sequences.            *
 *************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re (lenov@pasteur.fr)
      Receptors and Cognition 
      Pasteur Institute
      75724 Paris cedex 15, France
*/

/*>>>>>>>>>>>>>>>>>>>>>>PREPROCESSOR INFORMATIONS<<<<<<<<<<<<<<<<<<<<<<<<*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "consindex.h"

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/*****************
 * main function *
 *****************/

int main(int argc, char *argv[]){
    int count;                        /*loop counter*/
    
    for (count=1;count<argc;count++)
    {	
	switch (argv[count][1])
	{
	    case 'A':
		if (argv[count][2]=='\0') /*empty A option*/
		{
		    fprintf(stderr,"Bad option %s, no alignment filename\n", argv[count]);
		    usage();
		    exit(1);
		}
		if (argv[count][2]!='\0') /*valid A option*/
		    aln_ptr=&argv[count][2];
		break;
	    case 'M':
		if (argv[count][2]=='\0') /*empty M option*/
		{
		    fprintf(stderr,"Bad option %s, no matrix specified\n", argv[count]);
		    usage();
		    exit(1);
		}
		if (argv[count][2]!='\0') /*valid M option*/
		    mat_ptr=&argv[count][2];
		break;
	    case 'O':
		if (argv[count][2]=='\0') /*empty O option*/
		{
		    fprintf(stderr,"No outfile name specified  %s\n",argv[count]);
		    usage();
		    exit(1);
		}
		if (argv[count][2]!='\0') /*valid O option*/
		    out_ptr=&argv[count][2];
		break;
	    case 'V':
		verbose=1;		  /*turn on the verbose mode */
		break;
	    default:
		fprintf(stderr,"Bad option %s\n",argv[count]);
		usage();
		exit(1);
		break;
	}
    }
    fprintf(stdout,"***************************************************************\n");
    fprintf(stdout,"*                        ConsIndex v1.11                      *\n");
    fprintf(stdout,"*                 Nicolas Le Nov�re 1998-2004                 *\n");
    fprintf(stdout,"***************************************************************\n");
    
    InputAlign(aln_ptr);
    InputMatrix(mat_ptr);
    GlobSim();
    LocalSim();
    WriteResult();
    
    return EXIT_SUCCESS;
}

void usage(void)
{
    fprintf(stderr,"Usage is consindex -A[alignment.txt] -M[MATRIx] -O[output.txt]\n");
    fprintf(stderr,"options:\n");
    fprintf(stderr,"-A\tname of the file containing the sequence alignment\n");
    fprintf(stderr,"\tdefault is alignment.txt\n");
    fprintf(stderr,"\tthe last line of the alignment has to be empty\n");
    fprintf(stderr,"-M\tname of the comparison matrix\n");
    fprintf(stderr,"\tOnly the first five characters are evaluated\n");
    fprintf(stderr,"\tdefault is gcggap (other are pepident, nucident)\n");
    fprintf(stderr,"-O\tname of the result file\n");
    fprintf(stderr,"\tdefault is output.txt\n");
    fprintf(stderr,"-V\tturn on the verbose output\n");
}

void WriteResult(void)
{
    int hresidue,vresidue,seq,seq1,seq2,pos,i;	/*loop counters*/
    FILE *output;			        /*ptr on output file*/
    
    output=fopen(out_ptr,"w");
    if (output==NULL)
    {
	fprintf(stderr,"Unable to open output file\n");
	usage();
	exit(1);
    }

    if (verbose){
	/*--------*
	 | Header |
	 *--------*/
    
	fprintf(output,"***************************************************************\n");
	fprintf(output,"*                        ConsIndex v1.11                      *\n");
	fprintf(output,"*                  Nicolas Le Nov�re 1998-2004                *\n");
	fprintf(output,"***************************************************************\n");

	/*-----------*
	 | Alignment |
	 *-----------*/
    
	fprintf(output,"The alignment:\n");
	for (seq=0;seq<actualnum;seq++)
	    fprintf(output,"%s\t%s\n",alignment[seq].name,alignment[seq].sequence);
	fprintf(output,"\t");
	for (i=1;i<actualsize/10+1 ;i++)
	    fprintf(output,"%10d", 10*i);
	fprintf(output,"\n");
	
	/*-------------------*
	 | Similarity matrix |
	 *-------------------*/
    
	fprintf (output,"The matrix:\n");
	fprintf(output,"\t");
	for (hresidue=0;hresidue<actualres;hresidue++)
	    fprintf(output,"%c\t",usedmatrix.residue[hresidue]);
	fprintf(output,"\n");
	for (vresidue=0;vresidue<actualres;vresidue++)
	{
	    fprintf(output,"%c\t",usedmatrix.residue[vresidue]);
	    for (hresidue=0;hresidue<actualres;hresidue++)
		fprintf(output,"%d\t",usedmatrix.value[vresidue][hresidue]);
	    fprintf(output,"\n");
	}
    
	/*--------------------------------*
	 | Pairwise similarities in table |
	 *--------------------------------*/

	fprintf (output,"Pairwise global similarities:\n");
	for (seq1=0;seq1<actualnum;seq1++)
	{
	    fprintf(output,"%s\t",alignment[seq1].name);
	    for(seq2=0;seq2<actualnum;seq2++)
		fprintf(output,"%5.2f\t",GlobalSimilarity[seq1][seq2]);
	    fprintf(output,"\n");
	}
    
	/*----------------------------------*
	 | Pairwise similarities in columns |
	 *----------------------------------*/
	
	for (seq1=0;seq1<actualnum;seq1++)
	    for (seq2=seq1+1;seq2<actualnum;seq2++)
		fprintf(output,"%s\t%s\t%5.2f\n",alignment[seq1].name,alignment[seq2].name,GlobalSimilarity[seq1][seq2]);
    }
    /*-----------------------*
     | Position conservation |
     *-----------------------*/
    
    fprintf (output,"Local similarity:\n");
    for (pos=0;pos<actualsize;pos++)
	fprintf(output,"%d\t%5.2f\n",pos,LocalSimilarity[pos]);
    fclose(output);
}
