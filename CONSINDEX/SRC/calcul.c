/*************************************************************************
 *                               CALCUL.C                                *
 *************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re (lenov@pasteur.fr)
      Receptors and Cognition 
      Pasteur Institute
      75724 Paris cedex 15, France
*/

/*>>>>>>>>>>>>>>>>>>>>>>PREPROCESSOR INFORMATIONS<<<<<<<<<<<<<<<<<<<<<<<<*/

#include "calcul.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/*******************************
 * Calcul of global similarity *
 *******************************/

void GlobSim (void)
{
    int	seq1,seq2,pos;              //loop counters
    int	res,ind1=-1,ind2=-1;        //matrix indices
    int sum=0;			    //sum of similarities over the sequence
    int length=0;                   //length of non-doubled gap
    char AA1,AA2;		    //residue identity
    
    for (seq1=0; seq1<actualnum; seq1++)
	for (seq2=seq1;seq2<actualnum;seq2++)
	{
	    length=0,sum=0;
	    for (pos=0;pos<actualsize;pos++)
	    {
		AA1=alignment[seq1].sequence[pos];
		AA2=alignment[seq2].sequence[pos];
		if (AA1!='-' || AA2!='-')
		{
		    length++;
		    for (res=0;res<actualres;res++)
		    {
			if (AA1==usedmatrix.residue[res])
			    ind1=res;
			if (AA2==usedmatrix.residue[res])
			    ind2=res;
		    }
		    if (ind1==-1)
		    {
			fprintf(stderr,"illegal residu: %c, at position %d of sequence %d\n", AA1,pos,seq1);
			exit(1);
		    }
		    if (ind2==-1)
		    {
			fprintf(stderr,"illegal residu: %c, at position %d of sequence %d\n", AA2,pos,seq2);
			exit(1);
		    }
		    sum+=usedmatrix.value[ind1][ind2];
		}
	    }
	    GlobalSimilarity[seq1][seq2]=(double)sum/(double)length;
	}
}

/*********************************
 * Calcul of position similarity *
 *********************************/

void LocalSim (void)
{
    int	seq1,seq2,pos;              //loop counters
    int	res,ind1=-1,ind2=-1;        //matrix indices
    double sum=0;		    //sum of similarities for a position over the sequences
    double SumGlobal=0;             //sum of global DISIMILARITIES (1/similarities) over sequences	
    char AA1,AA2;		    //residue identity
    
    
    for (seq1=0; seq1<actualnum; seq1++)
	for (seq2=seq1+1;seq2<actualnum;seq2++)
	    SumGlobal += 1.0/GlobalSimilarity[seq1][seq2];
    for (pos=0;pos<actualsize;pos++)
    {
	sum=0;
	for (seq1=0; seq1<actualnum; seq1++)
	    for (seq2=seq1+1;seq2<actualnum;seq2++)
	    {
		AA1=alignment[seq1].sequence[pos];
		AA2=alignment[seq2].sequence[pos];
		if (AA1!='-' || AA2!='-')
		{
		    for (res=0;res<actualres;res++)
		    {
			if (AA1==usedmatrix.residue[res])
			    ind1=res;
			if (AA2==usedmatrix.residue[res])
			  ind2=res;
		    }
		    if (ind1==-1)
		    {
			fprintf(stderr,"illegal residu: %c, at position %d of sequence %d\n", AA1,pos,seq1);
			exit(1);
		    }
		    if (ind2==-1)
		    {
			fprintf(stderr,"illegal residu: %c, at position %d of sequence %d\n", AA2,pos,seq2);
			exit(1);
		    }
		    sum+=(double)usedmatrix.value[ind1][ind2]/GlobalSimilarity[seq1][seq2];
		}
	    }
	LocalSimilarity[pos]=sum/SumGlobal;
    }
}

