/*************************************************************************
 *                               CONSINDEX.H                             *
 *************************************************************************/
/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

      Nicolas Le Nov�re (lenov@pasteur.fr)
      Receptors and Cognition 
      Pasteur Institute
      75724 Paris cedex 15, France
*/

/* BAD: defined three times */
#define RESIDUES 22
#define SIZE 10001
#define NUMBER 301
#define NAME 50

/*>>>>>>>>>>>>>>>>>>>>>>>>>VARIABLES DEFINITIONS<<<<<<<<<<<<<<<<<<<<<<<<<*/

extern struct sequence {
    char name[NAME];	                 //names of sequences
    char sequence[SIZE];	         //sequence content
} alignment[NUMBER];                     //alignment itself

extern struct matrix{
	char namemat[21];                /*identification of the matrix*/
	char residue[RESIDUES];          /*identification of residues*/
	int  value[RESIDUES][RESIDUES];  /*similarity values [vertical][horizontal]*/
} usedmatrix;

extern int actualnum;			 //actual number of sequences 
extern int actualsize;			 //actual length of alignment
extern int actualres;			 //actual number of different residues

extern double GlobalSimilarity[NUMBER][NUMBER]; //Global similarity between sequences
extern double LocalSimilarity[SIZE];            //Local similarity for each alignment position

int  verbose=0;				 //long output if 1, short if 0
char *aln_ptr="alignment.txt";	         //input file name
char *out_ptr="output.txt";	         //output file name
char *mat_ptr="default";                 //matrix file name

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS PROTOTYPES<<<<<<<<<<<<<<<<<<<<<<<<*/

void usage(void);			 //precises the usage of the program
void WriteResult(void);			 //write the output file
extern void InputAlign(char *inputal);	 //read the alignment
extern void InputMatrix(char *inputmat); //read the matrix
extern void GlobSim (void);		 //computes global similarities between sequences
extern void LocalSim (void);		 //computes the conservation for each position

