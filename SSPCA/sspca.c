/*******************************************************************************
 *                                  SSPCA v1.3                                 *
 *  This program permits to combine predictions of protein secondary structure *
 *  coming from different algorithms and  several homologous sequences         *
 *                 Copyright (C) Nicolas Le Nov�re 1997-1999                   *
 *  Usage: SSPCA on the command line.                                          *
 *         -A[name of the lignment file]                                       *
 *         -P[name of the prediction file]                                     *
 *         -O[name of the output file]                                         *
 *  References:                                                                *
 *  File formats:                                                              *
 *     Alignment: Clustaw interleaved format. at least 1 space between seqname *
 *                and sequence and 1 space at the end of each line.            *
 *                The first line may contain anything. Every lines have to be  * 
 *                less than 100 characters                                     *
 *           CLUSTAL W(1.60) : My alignment                                    *
 *           SEQ1 MPATRTARRKK--EEGERS                                          *
 *           SEQ2 MP-SKSA----AADDIDKT                                          *
 *           ...                                                               *
 *           SEQN MPVSKSARKKRIIDEGDKS                                          *
 *                                                                             *
 *     Prediction:                                                             *
 *           # SEQ1                                                            *
 *           $ meth1                                                           *
 *           P HHHHEECCCCCEEEEEC                                               *
 *           H 99872000000212130                                               *
 *           P 11237648834678974                                               *
 *           C 00001468876220006                                               *
 *           s bbbbeeeebebebebeb                                               *
 *           S 01215675474737374                                               *
 *           T ooTTTTiiiiiTTTToo                                               *
 *           $ meth2                                                           *
 *  Restrictions: 50 sequences maximum. length of alignment: 1000, 50 methods  *
 *  Revision history: v1.0 29-10-1997 Nicolas Le Nov�re                        *
 *                    v1.1 10-12-1997 Nicolas Le Nov�re                        *
 *                    v1.2 31-12-1997 Nicolas Le Nov�re                        *
 *                    v1.3 04-03-1998 Nicolas Le Nov�re                        *
 *           Able to read a true Clustal alignment as input                    *
 *  Error handling:                                                            *
 *  Notes: All probabilities have to be expressed between 0 and 9              *
 *******************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

     Nicolas Le Nov��re 
     Neurobiologie Mol��culaire, Institut Pasteur, 25, rue du Dr ROUX, 
     75724 Paris cedex, FRANCE. e-mail: lenov@pasteur.fr

*/

/*>>>>>>>>>>>>>>>>>>>>>>>>>PREPROCESSOR INFORMATIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define NUMBER 50
#define SIZE 1000
#define PRED 10
/*#define DEBUG*/

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>VARIABLES DEFINITIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

int  actualsize=0,actualnum=0,actualpred=0;	/*actual parameters*/
char trash;									/*black hole*/
char *alignname="alignment.txt";			/*name of the alignment file*/
char *predname="prediction.txt";			/*name of the prediction file*/
char *outname="SSPCAout.txt";				/*name of the outfile file*/
char *spreadsheet="SSPCAout.dat";			/*name of the spreadsheet file*/
char data[NUMBER][PRED+1][7][SIZE+1];		/*contains alignment and projected pred*/				
char predmeth[PRED][10];					/*stores the prediction method name*/
char seqname[NUMBER][10];					/*stores the sequence names (alignment)*/
char seqlabel[NUMBER][10];					/*stores the sequence names (prediction)*/
char prediction[NUMBER][PRED][7][SIZE+1];	/*prediction array*/
struct state								
{
	char prediction;		/*P if the line exist, '\0' otherwise*/
	char probhelix;			/*H if the line exist, '\0' otherwise*/	
	char probsheet;			/*E if the line exist, '\0' otherwise*/		
	char probcoil;			/*C if the line exist, '\0' otherwise*/			
	char solvstate;			/*s if the line exist, '\0' otherwise*/		
	char solvprob;			/*S if the line exist, '\0' otherwise*/		
	char topology;			/*t if the line exist, '\0' otherwise*/		
};
struct state info[NUMBER][PRED];		    /*information about the presence or lack of prediction*/
struct consensus							/*processed data for 1 element*/
{
	int helix[SIZE];		/*Number of predicted helix*/
	int sheet[SIZE];		/*Number of predicted sheet*/
	int coil[SIZE];			/*Number of predicted coil*/
	int gap[SIZE];			/*Number of predicted gap(sic)*/
	char major[SIZE];		/*majority state*/
	int length;				/*length of the consensus (non-gap prediction)*/
	int alpha;				/*number of alpha helix residues*/
	int beta;				/*number of beta sheet residues*/
	double ratalp;			/*percent alpha helix residues*/
	double ratbet;			/*percent beta sheet residues*/
};
struct consensus seqproc[NUMBER];			/*by sequence (through methods)*/
struct consensus methproc[PRED];			/*by method (through sequences)*/
struct consensus total;						/*including sequences and methods*/
struct sum									/*processed data for 1 element*/
{
	int helix[SIZE];		/*sum of probability for helix state*/
	int sheet[SIZE];		/*sum of probability for sheet state*/
	int coil[SIZE];			/*sum of probability for coil state*/
};
struct sum seqsum[NUMBER];					/*by sequence (through methods)*/
struct sum methsum[PRED];					/*by method (through sequences)*/
struct sum totsum;							/*including sequences and methods*/
struct mat_elt								/*matrix element*/
{
	int total;				/*total length compared*/
	int identity;			/*total identical residues*/
	double percent;			/*identity ratio*/
};
struct mat_elt matrix[NUMBER*PRED][NUMBER*PRED];	/*Identity matrix*/
struct solvat
{
	int buried[SIZE];		/*Number of predicted buried residues*/
	int exposed[SIZE];		/*Number of predicted exposed residues*/
	int gap[SIZE];			/*Number of predicted gap(sic)*/
	char major[SIZE];		/*Main state of each aligned residu*/
	int prob[SIZE];			/*Probability to be exposed*/
} solvat;
struct topol
{
	int out[SIZE];			/*Number of predicted outer residues*/
	int in[SIZE];			/*Number of predicted inner residues*/
	int Trans[SIZE];		/*Number of predicted transmembrane residues*/
	int gap[SIZE];			/*Number of predicted gap(sic)*/
	char major[SIZE];		/*Main state of each aligned residu*/
} topol;
struct stat
{
	int sample;				/*sample size*/
	double mean;			/*mean of an array*/
	double stdev;			/*standard deviation of an array*/
	double sem;				/*standard deviation to the mean*/
};
struct stat methstat[PRED][PRED];			/*contains consistency between methods*/
struct stat seqstat[NUMBER][NUMBER];		/*contains consistency between sequences*/
struct stat relstat[PRED];					/*contains reliability of methods*/

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS PROTOTYPES<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

void usage(void);				/*precises the options of the programme*/
void ReadAlignment(void);		/*read the alignment and write the data array*/
void ReadPrediction(void);		/*read the predictions and write the pred array*/
void Projection(void);			/*projects the prediction on the data array*/
void Process(void);				/*computes quantity of H, E and C and consensus*/
void SumProb(void);				/*Sum of the probability of each state*/
char Majority(struct consensus *trucproc, int loc);
								/*computes majority structure for a position*/
void IdentityMatrix(void);		/*constructs the general identity matrix*/
void ComputIdentity(int sub1, int sub2, int pred1, int pred2);
								/*computes the element of an identity matrix*/
void MethCong (void);	        /*Congruence of methods (throughout sequences)*/
void SeqCong (void);	        /*Congruence of sequences (throughout methods)*/
void Consistency (void);		/*Consistency of each method*/
void Solvatation (void);		/*Compute the consensus solvatation state*/
char Majsolv(struct solvat *solvsum, int loc);
								/*computes majority solvatation state for a position*/
void SolvProb(void);			/*Sum of the solvatation probabilities*/
char Majtopol(struct topol *topsum, int loc);
void Topology(void);			/*Computes the consensus topological state*/
								/*Computes majority topological state for a position*/
struct stat DesStat (double *value_ptr);
								/*computes descriptive statistiqu of an array*/
void WriteOut(void);			/*write the results in output file*/

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FUNCTIONS<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/*****************
 * main function *
 *****************/

void main(int argc, char *argv[])
{
	int	count;			/*loop counter*/
	char *tmp_ptr;		/*temporary pointer to scren a string*/

#ifndef DEBUG
	fprintf(stdout,"******************************************************************\n");
        fprintf(stdout,"*                              SSPCA v1.3                        *\n");
        fprintf(stdout,"*       Copyright (C) Nicolas Le Nov�re 1997-1999 under GPL      *\n");
        fprintf(stdout,"* Combination of predictions of protein secondary structures     *\n");
        fprintf(stdout,"******************************************************************\n");
#endif /*DEBUG*/

	/*--------------------------------*
	 | Read the command-line options: |
	 | -A[alignment.txt]              |
	 | -P[prediction.txt]             |
	 | -O[outfile.txt]                |
	 *--------------------------------*/

	for (count=1;count<argc;count++)
	{	
		switch (argv[count][1])
		{
		case 'A':			/*name of the alignment file on the command line*/
			if (argv[count][2]=='\0') /*empty option*/
			{
				fprintf(stderr,"Bad option %s, no alignment filename\n",
					                                                argv[count]);
				usage();
			}
			if (argv[count][2]!='\0') /*valid option*/
				alignname=&argv[count][2];
			break;
		case 'P':			/*name of the prediction file on the command line*/
			if (argv[count][2]=='\0') /*empty option*/
			{
				fprintf(stderr,"Bad option %s, no prediction filename\n",
					                                                argv[count]);
				usage();
			}
			if (argv[count][2]!='\0') /*valid option*/
				predname=&argv[count][2];
			break;
		case 'O':			/*name of the output file on the command line*/
			if (argv[count][2]=='\0') /*empty option*/
			{
				fprintf(stderr,"Bad option %s\n",argv[count]);
				usage();
			}
			if (argv[count][2]!='\0') /*valid option*/
			{
				outname=&argv[count][2];
				strcpy(spreadsheet,outname);
				tmp_ptr=outname;
				while (*tmp_ptr!='\0')
				{
					if (*tmp_ptr=='.')
						tmp_ptr='\0';
					tmp_ptr++;
				}
				strcat(outname,".out");
				tmp_ptr=spreadsheet;
				while (*tmp_ptr!='\0')
				{
					if (*tmp_ptr=='.')
						tmp_ptr='\0';
					tmp_ptr++;
				}
				strcat(spreadsheet,".dat");
			}
			break;		
		default:
			fprintf(stderr,"Bad option %s\n",argv[count]);
			usage();
			break;
		}
	}

	memset(data,'\0',NUMBER*PRED*7*SIZE);
	memset(prediction,'\0',NUMBER*PRED*7*SIZE);
	memset(info,'\0',NUMBER*PRED*7);

	/*-------------------*
	 | input of the data |
	 *-------------------*/

	ReadAlignment();			/*read the aligment file*/
	ReadPrediction();			/*read the prediction file*/
	Projection();				/*project the secondary structure predictions 
	                                                           on the alignment*/

	/*------------------------*
	 | processing of the data |
	 *------------------------*/
	
	Process();					/*combine the data of the multiple alignment*/
	IdentityMatrix();			/*constructs an identity matrix*/
	Consistency();				/*reliability of each method*/
	SeqCong();				/*Consistency of sequence (throughout methods)*/
	MethCong();		        /*Consistency of method (throughout sequences)*/
	SumProb();					/*Sum of the probability of each state*/
	Solvatation();
	SolvProb();
	Topology();
	WriteOut();					/*print the results in the output file*/
}

/****************************************
 * precise the options of the programme *
 ****************************************/

void usage(void)
{
	fprintf(stderr,"Usage is SSPCA -A[alignment.txt] -P[prediction.txt] -O[outfile.txt]\n");
	fprintf(stderr,"options:\n");
	fprintf(stderr,"-A\tname of the file containing the sequence alignment\n");
	fprintf(stderr,"-P\tname of the file containing the secondary structure predictions\n");
	fprintf(stderr,"-O\tname of the result file\n");
}
	
/************************************************
 * read the alignment and write it in the array *
 ************************************************/

void ReadAlignment(void)		
{	
	int  seq=0,pos=0;		/*counters*/
	char line[100],seqline[100];
	FILE *inputal;			/*ptr on alignment file*/

	inputal=fopen(alignname,"r");

	/*-----------------------------------*
	 | Read the first line, with options |
	 *-----------------------------------*/

	fgets(line,sizeof(line),inputal);

	/*-------------------------*
	 | read the remaining file |
	 *-------------------------*/

	while(1)
	{
		fgets(line,sizeof(line),inputal);
		if (feof(inputal))
			break;
		if (line[0]!='\n' && line[0]!='\0' && line[0]!=' ')
		{
			sscanf(line,"%s %s",seqname[seq],seqline);
			strcat(data[seq][0][0],seqline);
#ifdef DEBUG
	fprintf(stdout,"entering the alignment\n");
	fprintf(stdout,"%s\n",data[seq][0][0]);
	fflush(stdout);
#endif
			seq++;
		}
		if (line[0]=='\n' || line[0]=='\0' || line[0]==' ')
			seq=0;
	}
	fclose(inputal);
	for (seq=0;seq<NUMBER;seq++)
		if (data[seq][0][0][0]!='\0')
			actualnum++;
	for (pos=0;pos<SIZE+1;pos++)
		if (data[0][0][0][pos]!='\0')
			actualsize++;

#ifdef DEBUG
	fprintf(stdout,"CLUSTAL:\n");
	for (seq=0;seq<actualnum;seq++)
		fprintf(stdout,"%s\n",data[seq][0][0]);
	fprintf(stdout,"actualnum:%d\n",actualnum);
	fprintf(stdout,"actualsize:%d\n",actualsize);
	fflush(stdout);
#endif
}

/*********************************************************
 * read the predictions and write them in the pred array *
 *********************************************************/

void ReadPrediction(void)		
{
	int	i=0,j=0,seq=0,seqin,meth=0,pos,count=0;		/*counters*/
	int pCoil;										/*integer version of probcoil*/
	char line[SIZE];
	FILE *inputpred;								/*ptr on prediction file*/

	inputpred=fopen(predname,"r");
	do
	{
		fgets(line,SIZE,inputpred);
		switch (line[0])
		{
			case '#':		/*sequence name on this line*/
				sscanf(line,"%c %s",&trash,seqlabel[seq]);
				for (j=0;j<actualnum;j++)
				{
					if(!strcmp(seqlabel[seq],seqname[j]))
					{
						seqin=j;
						break;
					}	
				}
				meth=0;
				seq++;
				break;
			case '$':	/*method name on this line*/
				sscanf(line,"%c %s",&trash,&predmeth[meth]);
				if ((meth+1)>actualpred)
					actualpred=meth+1;
				meth++;	/*This line shift meth BEFORE entering data, explaining the "meth-1" latter*/
				break;
			case 'P':	/*secondary structure prediction on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].prediction,&prediction[seqin][meth-1][0]);
				break;
			case 'H':	/*Helix probability on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].probhelix,&prediction[seqin][meth-1][1]);
				break;
			case 'E':	/*Beta probability on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].probsheet,&prediction[seqin][meth-1][2]);
				break;
			case 'C':	/*Coil probability on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].probcoil,&prediction[seqin][meth-1][3]);
				break;	
			case 's':	/*solvatation state on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].solvstate,&prediction[seqin][meth-1][4]);
				break;
			case 'S':	/*solvatation probability on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].solvprob,&prediction[seqin][meth-1][5]);
				break;
			case 't':   /*topology on this line*/
				sscanf(line,"%c %s",&info[seqin][meth-1].topology,&prediction[seqin][meth-1][6]);
				break;
			case ' ':   /*blank line*/
				break;
			default:	
				fprintf(stderr,"Illegal character found at first line position: %c",line[0]);
				exit(1);
				break;
		}
	} while (!feof(inputpred));

	for (seq=0;seq<actualnum;seq++)
		for (meth=0;meth<actualpred;meth++)
			if (info[seq][meth].probcoil=='\0'&&info[seq][meth].probsheet=='E'&&info[seq][meth].probhelix=='H')
			{
				pos=0;
				while (prediction[seq][meth][0][pos]!='\0')
				{
					pCoil=10-(prediction[seq][meth][1][pos]-48)-(prediction[seq][meth][2][pos]-48);
					if (pCoil<10)
						prediction[seq][meth][3][pos]=pCoil+48;
					if (pCoil>9)
						prediction[seq][meth][3][pos]='9';
					pos++;
				}
				info[seq][meth].probcoil='C';
			}
	fclose(inputpred);
#ifdef DEBUG
	fprintf(stdout,"actualpred:%d\n",actualpred);
	for (seq=0;seq<actualnum;seq++)
		for (meth=0;meth<actualpred;meth++)
		{
			fprintf(stdout,"%s %s:\n", seqname[seq],predmeth[meth]);
			fprintf(stdout,"%s\n",prediction[seq][meth][0]);
		}
	fflush(stdout);
#endif /*DEBUG*/
}

/*********************************************
 * projects the prediction on the data array *
 *********************************************/ 

void Projection(void)		
{
	int	seq,meth,aa,pos;		/*loop counters*/
	
	for (seq=0;seq<actualnum;seq++)
		for (meth=1;meth<actualpred+1;meth++)
		{
			aa=0,pos=0;
			if (prediction[seq][meth-1][0][pos]!='\0')
				while (data[seq][0][0][aa]!='\0')
				{/*gap insertion in prediction if it exists in alignment*/
					if (data[seq][0][0][aa]=='-')	 
					{	                                        
						if (info[seq][meth-1].prediction=='P')
							data[seq][meth][0][aa]='-';
						if (info[seq][meth-1].probhelix=='H')
							data[seq][meth][1][aa]='-';
						if (info[seq][meth-1].probsheet=='E')
							data[seq][meth][2][aa]='-';
						if (info[seq][meth-1].probcoil=='C')
							data[seq][meth][3][aa]='-';
						if (info[seq][meth-1].solvstate=='s')
							data[seq][meth][4][aa]='-';
						if (info[seq][meth-1].solvprob=='S')
							data[seq][meth][5][aa]='-';
						if (info[seq][meth-1].topology=='t')
							data[seq][meth][6][aa]='-';
						aa++;
					}
					if (data[seq][0][0][aa]!='-')
					{
						if (info[seq][meth-1].prediction=='P')
							data[seq][meth][0][aa]=prediction[seq][meth-1][0][pos];
						if (info[seq][meth-1].probhelix=='H')
							data[seq][meth][1][aa]=prediction[seq][meth-1][1][pos];
						if (info[seq][meth-1].probsheet=='E')
							data[seq][meth][2][aa]=prediction[seq][meth-1][2][pos];
						if (info[seq][meth-1].probcoil=='C')
							data[seq][meth][3][aa]=prediction[seq][meth-1][3][pos];
						if (info[seq][meth-1].solvstate=='s')
							data[seq][meth][4][aa]=prediction[seq][meth-1][4][pos];
						if (info[seq][meth-1].solvprob=='S')
							data[seq][meth][5][aa]=prediction[seq][meth-1][5][pos];
						if (info[seq][meth-1].topology=='t')
							data[seq][meth][6][aa]=prediction[seq][meth-1][6][pos];
						aa++,pos++;
					}
				}
		}
}


/*>>>>>>>>>>>>>>>>>>>>>>>>>TREATMENT OF SECONDARY STRUCTURE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/


/**************************************
 * compute the quantity of H, E and C *
 * and decide the consensus           *
 **************************************/

void Process(void)				
{
	int seq,pos,meth;			/*loop counters*/

	/*-----------------------------*
	 | initialisation of variables |
	 *-----------------------------*/

	for (seq=0;seq<actualnum;seq++)
	{
		seqproc[seq].length=0;
		seqproc[seq].alpha=0;
		seqproc[seq].beta=0;
		memset(seqproc[seq].helix,0,actualsize);
		memset(seqproc[seq].sheet,0,actualsize);
		memset(seqproc[seq].coil,0,actualsize);
		memset(seqproc[seq].gap,0,actualsize);
			}
	for (meth=0;meth<actualpred;meth++)
	{
		methproc[meth].length=0;
		methproc[meth].alpha=0;
		methproc[meth].beta=0;
		memset(methproc[meth].helix,0,actualsize);
		memset(methproc[meth].sheet,0,actualsize);
		memset(methproc[meth].coil,0,actualsize);
		memset(methproc[meth].gap,0,actualsize);
	}
	total.length=0;
	total.alpha=0;
	total.beta=0;
	memset(total.helix,0,actualsize);
	memset(total.sheet,0,actualsize);
	memset(total.coil,0,actualsize);
	memset(total.gap,0,actualsize);

	/*--------------------------------------------------------*
	 | processes data by sequence (consensus through methods) |
	 *--------------------------------------------------------*/

	for (seq=0;seq<actualnum;seq++)
	{
		for (pos=0;pos<actualsize;pos++)
		{	
			for (meth=0;meth<actualpred;meth++)
				if (info[seq][meth].prediction=='P')
					switch (data[seq][meth+1][0][pos])
					{
						case 'H':
							seqproc[seq].helix[pos]++;
							break;
						case 'E':
							seqproc[seq].sheet[pos]++;
							break;
						case 'C':
							seqproc[seq].coil[pos]++;
							break;
						case '-':
							seqproc[seq].gap[pos]++;
							break;
						case '\0': /*missing prediction*/
							break;
						default:
							fprintf(stderr,"Illegal struct character prediction: %c\n",data[seq][meth+1][0][pos]);
							fprintf(stderr,"sequence: %s, method: %s, position: %d\n",seqname[seq],predmeth[meth],pos);
							exit(1);
							break;
					}
			seqproc[seq].major[pos]=Majority(&seqproc[seq],pos);	
			switch (seqproc[seq].major[pos])
			{
				case 'H':
					seqproc[seq].alpha++;
					seqproc[seq].length++;
					break;
				case 'E':
					seqproc[seq].beta++;
					seqproc[seq].length++;
					break;
				case 'C':
					seqproc[seq].length++;
					break;
				case '-':
					break;
				default:
					fprintf(stderr,"Illegal majority prediction: %c",seqproc[seq].major[pos]);
					exit(1);
					break;
			}
		}
		seqproc[seq].ratalp=((double)seqproc[seq].alpha/(double)seqproc[seq].length)*100;
		seqproc[seq].ratbet=((double)seqproc[seq].beta/(double)seqproc[seq].length)*100;
	}
		
	/*--------------------------------------------------------*
	 | processed data by method (consensus through sequences) |
	 *--------------------------------------------------------*/

	for (meth=0;meth<actualpred;meth++)
	{
		for (pos=0;pos<actualsize;pos++)
		{	
			for (seq=0;seq<actualnum;seq++)
				if (info[seq][meth].prediction=='P')
					switch (data[seq][meth+1][0][pos])
					{
						case 'H':
							methproc[meth].helix[pos]++;
							break;
						case 'E':
							methproc[meth].sheet[pos]++;
							break;
						case 'C':
							methproc[meth].coil[pos]++;
							break;
						case '-':
							methproc[meth].gap[pos]++;
							break;
						case '\0': /*missing prediction*/
							break;
						default:
							fprintf(stderr,"Illegal struct character prediction: %c",data[seq][meth+1][0][pos]);
							fprintf(stderr,"sequence: %s, method: %s, position: %d\n",seqname[seq],predmeth[meth],pos);
							exit(1);
							break;
					}
			methproc[meth].major[pos]=Majority(&methproc[meth],pos);
			switch (methproc[meth].major[pos])
			{
				case 'H':
					methproc[meth].alpha++;
					methproc[meth].length++;
					break;
				case 'E':
					methproc[meth].beta++;
					methproc[meth].length++;
					break;
				case 'C':
					methproc[meth].length++;
					break;
				case '-':
					break;
				default:
					fprintf(stderr,"Illegal majority prediction: %c",seqproc[seq].major[pos]);
					exit(1);
					break;
			}
			methproc[meth].ratalp=((double)methproc[meth].alpha/(double)methproc[meth].length)*100;
			methproc[meth].ratbet=((double)methproc[meth].beta/(double)methproc[meth].length)*100;
		}
	}

	/*--------------------------------------------------------*
	 | processed data including all sequences and all methods |
	 *--------------------------------------------------------*/
	
	for (pos=0;pos<actualsize;pos++)
	{
		for (seq=0;seq<actualnum;seq++)
			for (meth=0;meth<actualpred;meth++)
				if (info[seq][meth].prediction=='P')
					switch (data[seq][meth+1][0][pos])
					{
						case 'H':
							total.helix[pos]++;
							break;
						case 'E':
							total.sheet[pos]++;
							break;
						case 'C':
							total.coil[pos]++;
							break;
						case '-':
							total.gap[pos]++;
							break;
						case '\0': /*missing prediction*/
							break;
						default:
							fprintf(stderr,"Illegal struct character prediction: %c",data[seq][meth+1][0][pos]);
							fprintf(stderr,"sequence: %s, method: %s, position: %d\n",seqname[seq],predmeth[meth],pos);
							exit(1);
							break;
					}
		total.major[pos]=Majority(&total,pos);
		switch (total.major[pos])
		{
			case 'H':
				total.alpha++;
				total.length++;
				break;
			case 'E':
				total.beta++;
				total.length++;
				break;
			case 'C':
				total.length++;
				break;
			case '-':
				break;
			default:
				fprintf(stderr,"Illegal majority prediction: %c",seqproc[seq].major[pos]);
				exit(1);
				break;
		}
		total.ratalp=((double)total.alpha/(double)total.length)*100;
		total.ratbet=((double)total.beta/(double)total.length)*100;
	}
}

/******************************************************
 * compute the majority structure at a given position *
 ******************************************************/

char Majority(struct consensus *trucproc, int loc)
{
	char state;	
	int  max;

	state='-';
	max=(*trucproc).gap[loc];

	if((*trucproc).coil[loc]>=max)
	{
		max=(*trucproc).coil[loc];
		state='C';
	}
	if((*trucproc).helix[loc]>=max)
	{
		max=(*trucproc).helix[loc];
		state='H';
	}
	if((*trucproc).sheet[loc]>=max)
	{
		max=(*trucproc).sheet[loc];
		state='E';
	}
	return(state);
}


/****************************************
 * Sum of the probability of each state *
 ****************************************/

 void SumProb(void)
 {
	int seq,meth,pos;		/*counters*/
	char dest[2];
	char *H_ptr,*E_ptr,*C_ptr;

 	/*-----------------------------*
	 | initialisation of variables |
	 *-----------------------------*/

	for (seq=0;seq<actualnum;seq++)
	{
		memset(seqsum[seq].helix,0,actualsize);
		memset(seqsum[seq].sheet,0,actualsize);
		memset(seqsum[seq].coil,0,actualsize);
	}
	for (meth=0;meth<actualpred;meth++)
	{
		memset(methsum[meth].helix,0,actualsize);
		memset(methsum[meth].sheet,0,actualsize);
		memset(methsum[meth].coil,0,actualsize);
	}
	memset(totsum.helix,0,actualsize);
	memset(totsum.sheet,0,actualsize);
	memset(totsum.coil,0,actualsize);

 	/*--------------------------------------------------------------*
	 | sum of probabilities by sequence (consensus through methods) |
	 *--------------------------------------------------------------*/
	for (seq=0;seq<actualnum;seq++)
		for (meth=0;meth<actualpred;meth++)
		{	
			/*meth+1 because 0 is the sequence*/
			H_ptr=data[seq][meth+1][1];
			E_ptr=data[seq][meth+1][2];
			C_ptr=data[seq][meth+1][3];
			for(pos=0;pos<actualsize;pos++)
			{
				if (info[seq][meth].probhelix=='H')
				{
					strncpy(dest,H_ptr,1);
					H_ptr++;
					seqsum[seq].helix[pos]+=atoi(dest);
				}
				if (info[seq][meth].probsheet=='E')
				{
					strncpy(dest,E_ptr,1);
					E_ptr++;
					seqsum[seq].sheet[pos]+=atoi(dest);
				}
				if (info[seq][meth].probcoil=='C')
				{
					strncpy(dest,C_ptr,1);
					C_ptr++;
					seqsum[seq].coil[pos]+=atoi(dest);
				}
			}
		}

	/*--------------------------------------------------------------*
	 | sum of probabilities by method (consensus through sequences) |
	 *--------------------------------------------------------------*/
	for (meth=0;meth<actualpred;meth++)
		for (seq=0;seq<actualnum;seq++)
		{	/*meth+1 because 0 is the sequence*/
			H_ptr=data[seq][meth+1][1];
			E_ptr=data[seq][meth+1][2];
			C_ptr=data[seq][meth+1][3];
			for(pos=0;pos<actualsize;pos++)
			{
				if (info[seq][meth].probhelix=='H')
				{
					strncpy(dest,H_ptr,1);
					H_ptr++;
					methsum[meth].helix[pos]+=atoi(dest);
				}
				if (info[seq][meth].probsheet=='E')
				{
					strncpy(dest,E_ptr,1);
					E_ptr++;
					methsum[meth].sheet[pos]+=atoi(dest);
				}
				if (info[seq][meth].probcoil=='C')
				{
					strncpy(dest,C_ptr,1);
					C_ptr++;
					methsum[meth].coil[pos]+=atoi(dest);
				}
			}
		}

	/*--------------------------------------------------------------*
	 | sum of probabilities including all sequences and all methods |
	 *--------------------------------------------------------------*/

	for (meth=0;meth<actualpred;meth++)
	{
		for (pos=0;pos<actualsize;pos++)
		{
			totsum.helix[pos]+=methsum[meth].helix[pos];
			totsum.sheet[pos]+=methsum[meth].sheet[pos];
			totsum.coil[pos]+=methsum[meth].coil[pos];
		}
	}
 }
 

/**************************************
 * Computation of the identity scores *
 **************************************/

/*-----------------------------------------*
 | constructs the general identity matrix |
 *-----------------------------------------*/

void IdentityMatrix(void)
{
	int seq1,seq2,meth1,meth2;

	for (seq1=0;seq1<actualnum;seq1++)
		for (meth1=0;meth1<actualpred;meth1++)
			for (seq2=0;seq2<actualnum;seq2++)
				for (meth2=0;meth2<actualpred;meth2++)
					ComputIdentity(seq1,seq2,meth1,meth2);
} 

/*--------------------------------------------*
 | computes the element of an identity matrix |
 *--------------------------------------------*/

void ComputIdentity(int sub1, int sub2, int pred1, int pred2)
{
	int pos;

	/*initialisation of the matrix element*/
	matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].identity=0;
	matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].total=0;
	matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].percent=0.0;

	/*screen along the alignment*/
	for (pos=0;pos<actualsize;pos++)
	{	/*CAUTION: data[][pred+1] because sequences in the first row*/
		if ((data[sub1][pred1+1][0][pos]!='-')&&(data[sub2][pred2+1][0][pos]!='-'))
		{
			matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].total++;
			if (data[sub1][pred1+1][0][pos]==data[sub2][pred2+1][0][pos])
				matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].identity++;
		}
	}
	/*in case of prediction lacking, avoids division by zero*/
	if (matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].total==0)
		matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].percent=0.0;
	else
		matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].percent
		=100*(double)matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].identity
		/(double)matrix[sub1*actualpred+pred1][sub2*actualpred+pred2].total;
}

/****************************
 * Consistency calculations *
 ****************************/

/*------------------------*
 | Consistency of methods |
 *------------------------*/

void Consistency(void)
{
	int seq1,seq2,meth,i;

	double value[PRED][(NUMBER*NUMBER)-NUMBER];

	for (meth=0;meth<actualpred;meth++)
	{
		for (i=0,seq1=0;seq1<actualnum;seq1++)
			for (seq2=seq1;seq2<actualnum;seq2++,i++)
				value[meth][i]=matrix[seq1*actualpred+meth][seq2*actualpred+meth].percent;
		value[meth][i]=-1.0;
		relstat[meth]=DesStat(value[meth]);
	}
}

/*---------------------------------------------------*
 | Congruence between sequences (throughout methods) |
 *---------------------------------------------------*/

void SeqCong (void)
{
	int i,seq1,seq2,meth;
	double value[NUMBER][NUMBER][PRED];

	for (seq1=0;seq1<actualnum;seq1++)
		for (seq2=0;seq2<actualnum;seq2++)
		{	
			for (i=0,meth=0;meth<actualpred;meth++,i++)
				value[seq1][seq2][i]=matrix[seq1*actualpred+meth][seq2*actualpred+meth].percent;
			value[seq1][seq2][i]=-1.0;
			seqstat[seq1][seq2]=DesStat(value[seq1][seq2]);
#ifdef DEBUG
			fprintf(stdout,"congruence between sequences %d and %d\n",seq1, seq2);
			fprintf(stdout,"%f\t%f\n",seqstat[seq1][seq2].mean,seqstat[seq1][seq2].stdev);
			fflush(stdout);
#endif /*DEBUG*/
		}
}

/*--------------------------------------------------*
 | Congruence between method (throughout sequences) |
 *--------------------------------------------------*/

void MethCong(void)
{
	int i,meth1,meth2,seq;
	double value[PRED][PRED][NUMBER];

	for (meth1=0;meth1<actualpred;meth1++)
		for (meth2=0;meth2<actualpred;meth2++)
		{	
			for (i=0,seq=0;seq<actualnum;seq++,i++)
				value[meth1][meth2][i]=matrix[seq*actualpred+meth1][seq*actualpred+meth2].percent;
			value[meth1][meth2][i]=-1.0;
			methstat[meth1][meth2]=DesStat(value[meth1][meth2]);
#ifdef DEBUG
			fprintf(stdout,"congruence between methods %d and %d\n", meth1, meth2);
			fprintf(stdout,"%f\t%f\n",methstat[meth1][meth2].mean,methstat[meth1][meth2].stdev);
			fflush(stdout);
#endif /*DEBUG*/
		}
}





/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TREATMENT OF SOLVATATION STATE<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/********************************************
 * Computes the consensus solvatation state *
 ********************************************/

void Solvatation (void)		
{
	int seq,meth,pos;	/*loop counters*/

	memset(solvat.buried,0,actualsize);
	memset(solvat.exposed,0,actualsize);
	memset(solvat.prob,0,actualsize);

	for (pos=0;pos<actualsize;pos++)
	{
		for (seq=0;seq<actualnum;seq++)
			for (meth=0;meth<actualpred;meth++)
				if (info[seq][meth].solvstate=='s')
					switch (data[seq][meth+1][4][pos])
					{
						case 'b':
							solvat.buried[pos]++;
							break;
						case 'e':
							solvat.exposed[pos]++;
							break;
						case '-':
							solvat.gap[pos]++;
							break;
						case '\0': /*missing prediction*/
							break;
						default:
							fprintf(stderr,"Illegal solvat character prediction: %c\n",data[seq][meth+1][4][pos]);
							fprintf(stderr,"sequence: %s, method: %s, position: %d\n",seqname[seq],predmeth[meth],pos);
							exit(1);
							break;
					}
		solvat.major[pos]=Majsolv(&solvat,pos);	
	}
}

/******************************************************
 * computes majority solvatation state for a position *
 ******************************************************/

char Majsolv(struct solvat *solvsum, int loc)
{
	char state;	
	int  max;

	state='-';
	max=(*solvsum).gap[loc];

	if((*solvsum).exposed[loc]>=max)
	{
		max=(*solvsum).exposed[loc];
		state='e';
	}
	if((*solvsum).buried[loc]>=max)
	{
		max=(*solvsum).buried[loc];
		state='b';
	}
	return(state);
}

/****************************************
 * Sum of the solvatation probabilities *
 ****************************************/

void SolvProb(void)		
{
	int seq,meth,pos;		/*counters*/
	char dest[2];
	char *S_ptr;

	memset(solvat.prob,0,actualsize);

	for (pos=0;pos<actualsize;pos++)
		for (seq=0;seq<actualnum;seq++)
			if (info[seq][meth].solvprob=='S')
			{
				S_ptr=data[seq][meth+1][5];
				for (meth=0;meth<actualpred;meth++)
				{
					strncpy(dest,S_ptr,1);
					S_ptr++;
					solvat.prob[pos]+=atoi(dest);
				}
			}
 }
 
								
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TREATMENT OF TOPOLOGY<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/********************************************
 * Computes the consensus topological state *
 ********************************************/

void Topology (void)		
{
	int seq,meth,pos;	/*loop counters*/

	memset(topol.out,0,actualsize);
	memset(topol.in,0,actualsize);
	memset(topol.Trans,0,actualsize);
	memset(topol.gap,0,actualsize);

	for (pos=0;pos<actualsize;pos++)
	{
		for (seq=0;seq<actualnum;seq++)
			for (meth=0;meth<actualpred;meth++)
				if (info[seq][meth].topology=='t')
					switch (data[seq][meth+1][6][pos])
					{
						case 'o':
							topol.out[pos]++;
							break;
						case 'i':
							topol.in[pos]++;
							break;
						case 'T':
							topol.Trans[pos]++;
							break;
						case '-':
							topol.gap[pos]++;
							break;
						case '\0': /*missing prediction*/
							break;
						default:
							fprintf(stderr,"Illegal topol character prediction: %c\n",data[seq][meth+1][6][pos]);
							fprintf(stderr,"sequence: %s, method: %s, position: %d\n",seqname[seq],predmeth[meth],pos);
							exit(1);
							break;
					}
		topol.major[pos]=Majtopol(&topol,pos);	
	}
}

/******************************************************
 * computes majority topological state for a position *
 ******************************************************/

char Majtopol(struct topol *topsum, int loc)
{
	char state;	
	int  max;

	state='-';
	max=(*topsum).gap[loc];

	if((*topsum).out[loc]>=max)
	{
		max=(*topsum).out[loc];
		state='o';
	}
	if((*topsum).in[loc]>=max)
	{
		max=(*topsum).in[loc];
		state='i';
	}
	if((*topsum).Trans[loc]>=max)
	{
		max=(*topsum).Trans[loc];
		state='T';
	}
	return(state);
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

/********************
 * Mean, SD and sem *
 ********************/

struct stat DesStat (double *value_ptr)
{
	int	 number=0;		/*population*/
	double sum=0.0;		/*sum of values*/
	double sumsq=0.0;	/*sum of squared values*/
	double sumdist=0.0;	/*sum of dist to the mean*/
	double *temp_ptr;
	struct stat statvalue;

	temp_ptr=value_ptr;
	while (*temp_ptr!=-1)
	{
		if (*temp_ptr!=0.0)
			number++;
		sum+=*temp_ptr;
		sumsq+=(*temp_ptr)*(*temp_ptr);
		
		temp_ptr++;
	}
	statvalue.sample=number;
	statvalue.mean=sum/(double)number;
	temp_ptr=value_ptr;
	while (*temp_ptr>=0)
	{
		sumdist+=((*temp_ptr)-statvalue.mean)*((*temp_ptr)-statvalue.mean);
		temp_ptr++;
	}
	if (sumdist>=0)
		statvalue.sem=sqrt(sumdist/((double)number*(double)number));
	statvalue.stdev = sqrt( ( (double)number*sumsq - sum*sum ) / ( (double)number * (double)number ) );
	return (statvalue);
}

/*****************************************
 * Write the results in the output files *
 *****************************************/

void WriteOut(void)
{
	int	seq,seq1,seq2,meth,meth1,meth2,pos;	/*loop counters*/
	FILE *outfile;							/*ptr on the result file*/
	FILE *sprsht;							/*ptr on the spradsheet file*/
	
	outfile=fopen(outname,"w");

	/*-------------------*
	 | prints the header |
	 *-------------------*/

	fprintf(outfile,"********************************************\n");
	fprintf(outfile,"*               SSPCA v1.3                 *\n");
	fprintf(outfile,"*        Nicolas Le Novere 1997,98         *\n");
	fprintf(outfile,"********************************************\n");

	/*----------------------*
	 | prints various items |
     *----------------------*/

	fprintf(outfile,"number of sequences: \t%d:\n",actualnum);
	fprintf(outfile,"number of methods: \t%d:\n",actualpred);
	fprintf(outfile,"length of alignment: \t%d:\n",actualsize);

	/*----------------------*
	 | prints the alignment |
	 *----------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"initial alignment:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"%d %d\n",actualnum,actualsize);
	for (seq=0;seq<actualnum;seq++)
	{
		fprintf(outfile,"%s\t",seqname[seq]);
		fprintf(outfile,"%s\n",data[seq][0][0]);
	}
	fprintf(outfile,"\n");
	for (pos=0;pos<actualsize+20;pos++)
		fprintf(outfile,"*");

    /*------------------------*
	 | prints the predictions |
	 *------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"predictions:\n");
	fprintf(outfile,"^^^^^^^^^^^^\n");
	fprintf(outfile,"\n");
	for (seq=0;seq<actualnum;seq++)
		for(meth=0;meth<actualpred;meth++)
		{	
			fprintf(outfile,"%s\t",predmeth[meth]);
			fprintf(outfile,"%s\n",prediction[seq][meth][0]);
		}
	fprintf(outfile,"\n");
	for (pos=0;pos<actualsize+20;pos++)
		fprintf(outfile,"*");

	/*----------------------------------*
	 | prints the projected predictions |
	 *----------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"projected projection:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"\n");
	for (seq=0;seq<actualnum;seq++)
	{
		fprintf(outfile,"%s\t",seqname[seq]);
		fprintf(outfile,"%s\n",data[seq][0]);
		for(meth=1;meth<actualpred+1;meth++)
		{
			if (info[seq][meth-1].prediction=='P')
				fprintf(outfile,"%s\t\t",predmeth[meth-1]);
				fprintf(outfile,"%s\n",data[seq][meth][0]);
			if (info[seq][meth-1].probhelix=='H')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][1]);
			if (info[seq][meth-1].probsheet=='E')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][2]);
			if (info[seq][meth-1].probcoil=='C')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][3]);
			if (info[seq][meth-1].solvstate=='s')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][4]);
			if (info[seq][meth-1].solvprob=='S')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][5]);
			if (info[seq][meth-1].topology=='t')
				fprintf(outfile,"\t\t%s\n",data[seq][meth][6]);
		}
	}
	fprintf(outfile,"\n");
	for (pos=0;pos<actualsize+20;pos++)
		fprintf(outfile,"*");

    /*----------------------------*
	 | prints the identity matrix |
	 *----------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"identity matrix:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"data expressed as total common length, identical positions, percent identity\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"|");
	for (seq=0;seq<actualnum;seq++)
	{
		fprintf(outfile,"%7s|",seqname[seq]);
		for (meth=0;meth<actualpred-1;meth++)
			fprintf(outfile,"       |");
	}
	fprintf(outfile,"\n");
	fprintf(outfile,"|");
	for (seq=0;seq<actualnum;seq++)
		for (meth=0;meth<actualpred;meth++)
			fprintf(outfile,"%7s|",predmeth[meth]);
	fprintf(outfile,"\n-");	
	for (pos=0;pos<(actualnum*actualpred)*8;pos++)
		fprintf(outfile,"-");
	fprintf(outfile,"\n");	
	for (meth1=0;meth1<(actualnum*actualpred);meth1++)
		{
			fprintf(outfile,"|");
			for (meth2=0;meth2<(actualnum*actualpred);meth2++)
			{
				if (meth2<meth1)
					fprintf(outfile,"       |");
				else
					fprintf(outfile,"  %3d  |",matrix[meth1][meth2].total);
			}
			fprintf(outfile,"\n|");
			for (meth2=0;meth2<(actualnum*actualpred);meth2++)
			{
				if (meth2<meth1)
					fprintf(outfile,"       |");
				else
					fprintf(outfile,"  %3d  |",matrix[meth1][meth2].identity);
			}
			fprintf(outfile,"\n|");
			for (meth2=0;meth2<(actualnum*actualpred);meth2++)
			{
				if (meth2<meth1)
					fprintf(outfile,"       |");
				else
					fprintf(outfile,"%6.2f |",matrix[meth1][meth2].percent);
			}
			fprintf(outfile,"\n-");
			for (pos=0;pos<(actualnum*actualpred)*8;pos++)
				fprintf(outfile,"-");
			fprintf(outfile,"\n");
		}
	for (pos=0;pos<actualsize+20;pos++)
	fprintf(outfile,"*");

	/*-------------------------------*
     | Prints the method consistency |
	 *-------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"method consistency:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"data are expressed as mean and SD\n");
		fprintf(outfile,"\n");
	for (meth=0;meth<actualpred;meth++)
	{
		fprintf(outfile,"%s\t",predmeth[meth]);
		fprintf(outfile,"%5.2f%% (%5.2f)\n",relstat[meth].mean,relstat[meth].stdev);
	}

	/*---------------------------------------*
     | Prints the congruence between methods |
	 *---------------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"congruence between methods:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"data are expressed as mean and SD\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"|");
	for (meth=0;meth<actualpred;meth++)
		fprintf(outfile,"%7s |",predmeth[meth]);
	fprintf(outfile,"\n-");	
	for (pos=0;pos<(actualnum*actualpred)*9;pos++)
		fprintf(outfile,"-");
	fprintf(outfile,"\n");	
	for (meth1=0;meth1<actualpred;meth1++)
	{
		fprintf(outfile,"|");
		for(meth2=0;meth2<actualpred;meth2++)
		{
			if (meth2<meth1)
				fprintf(outfile,"        |");
			else
				fprintf(outfile," %6.2lf |",methstat[meth1][meth2].mean);
		}
		fprintf(outfile,"\n|");
		for(meth2=0;meth2<actualpred;meth2++)
		{
			if (meth2<meth1)
				fprintf(outfile,"        |");
			else
				fprintf(outfile," %6.2lf |",methstat[meth1][meth2].stdev);
		}
		fprintf(outfile,"\n-");
		for (pos=0;pos<actualpred*9;pos++)
			fprintf(outfile,"-");
		fprintf(outfile,"\n");
	}
	for (pos=0;pos<actualsize+20;pos++)
	fprintf(outfile,"*");


	/*---------------------------------------------*
     | Prints the the congruence between sequences |
	 *---------------------------------------------*/
	fprintf(outfile,"\n");
	fprintf(outfile,"congruence between sequences:\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"data are expressed as mean and SD\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"|");
	for (seq=0;seq<actualnum;seq++)
		fprintf(outfile,"%7s |",seqname[seq]);
	fprintf(outfile,"\n-");	
	for (pos=0;pos<(actualnum*actualpred)*9;pos++)
		fprintf(outfile,"-");
	fprintf(outfile,"\n");	
	for (seq1=0;seq1<actualnum;seq1++)
	{
		fprintf(outfile,"|");
		for(seq2=0;seq2<actualnum;seq2++)
		{
			if (seq2<seq1)
				fprintf(outfile,"        |");
			else
				fprintf(outfile," %6.2lf |",seqstat[seq1][seq2].mean);
		}
		fprintf(outfile,"\n|");
		for(seq2=0;seq2<actualnum;seq2++)
		{
			if (seq2<seq1)
				fprintf(outfile,"        |");
			else
				fprintf(outfile," %6.2lf |",seqstat[seq1][seq2].stdev);
		}
		fprintf(outfile,"\n-");
		for (pos=0;pos<actualnum*9;pos++)
			fprintf(outfile,"-");
		fprintf(outfile,"\n");
	}
	for (pos=0;pos<actualsize+20;pos++)
	fprintf(outfile,"*");


	/*--------------------------------------------*
	 | prints the predicted structure by sequence |
	 *--------------------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"Consensus structure throughout methods\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
	for (seq=0;seq<actualnum;seq++)
	{
		fprintf(outfile,"%s \t",seqname[seq]);
		fprintf(outfile,"%s",seqproc[seq].major);
		fprintf(outfile,"\n");
		fprintf(outfile,"length=%d, ",seqproc[seq].length);
		fprintf(outfile,"helix=%d (%5.2lf%%), ",seqproc[seq].alpha,seqproc[seq].ratalp);
		fprintf(outfile,"beta=%d (%5.2lf%%)\n",seqproc[seq].beta,seqproc[seq].ratbet);
	}
	fprintf(outfile,"\n");

	/*------------------------------------------*
	 | prints the predicted structure by method |
	 *------------------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"Consensus structure throughout sequences\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
	for (meth=0;meth<actualpred;meth++)
	{
		fprintf(outfile,"%s \t",predmeth[meth]);
		fprintf(outfile,"%s",methproc[meth].major);
		fprintf(outfile,"\n");
		fprintf(outfile,"length=%d, ",methproc[meth].length);
		fprintf(outfile,"helix=%d (%5.2lf%%), ",methproc[meth].alpha,methproc[meth].ratalp);
		fprintf(outfile,"beta=%d (%5.2lf%%)\n",methproc[meth].beta,methproc[meth].ratbet);
	}
	fprintf(outfile,"\n");
	
	/*--------------------------------*
	 | prints the consensus structure |
	 *--------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"Consensus structure\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"\t%s\n",total.major);	
	fprintf(outfile,"\n");
	fprintf(outfile,"length=%d, ",total.length);
	fprintf(outfile,"helix=%d (%5.2lf%%), ",total.alpha, total.ratalp);
	fprintf(outfile,"beta=%d (%5.2lf%%)\n",total.beta,total.ratbet);
	fprintf(outfile,"\n");

	/*------------------------------*
	 | prints the solvatation state |
	 *------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"Solvatation state\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"\t%s\n",solvat.major);
	fprintf(outfile,"\n");

	/*------------------------------*
	 | prints the topological state |
	 *------------------------------*/

	fprintf(outfile,"\n");
	fprintf(outfile,"topological state\n");
	fprintf(outfile,"^^^^^^^^^^^^^^^^^\n");
	fprintf(outfile,"\t%s\n",topol.major);
	fprintf(outfile,"\n");

	fclose(outfile);

    /*--------------------------------------------------*
	 | prints the spreadsheet file for graphical display |
	 *--------------------------------------------------*/

	sprsht=fopen(spreadsheet,"w");
	fprintf(sprsht,"by sequence\t\t\t\t");
	for (seq=0;seq<actualnum-1;seq++)
		fprintf(sprsht,"\t\t\t\t");
	fprintf(sprsht,"by method\t\t\t\t");
	for (meth=0;meth<actualpred-1;meth++)
		fprintf(sprsht,"\t\t\t\t");
	fprintf(sprsht,"total\n");
	for (seq=0;seq<actualnum;seq++)
		fprintf(sprsht,"%s\t\t\t\t",seqname[seq]);
	for (meth=0;meth<actualpred;meth++)
		fprintf(sprsht,"%s\t\t\t\t",predmeth[meth]);
	fprintf(sprsht,"\n");
	for (seq=0;seq<actualnum+actualpred+1;seq++)
		fprintf(sprsht,"H\tE\tC\t\t");
	fprintf(sprsht,"\n");
	for (pos=0;pos<actualsize;pos++)
	{
		for (seq=0;seq<actualnum;seq++)
			fprintf(sprsht,"%d\t%d\t%d\t\t",seqproc[seq].helix[pos],seqproc[seq].sheet[pos],seqproc[seq].coil[pos]);
		for (meth=0;meth<actualpred;meth++)
			fprintf(sprsht,"%d\t%d\t%d\t\t",methproc[meth].helix[pos],methproc[meth].sheet[pos],methproc[meth].coil[pos]);
		fprintf(sprsht,"%d\t%d\t%d",total.helix[pos],total.sheet[pos],total.coil[pos]);
		fprintf(sprsht,"\n");
	}
	fprintf(sprsht,"by sequence\t\t\t\t");
	for (seq=0;seq<actualnum-1;seq++)
		fprintf(sprsht,"\t\t\t\t");
	fprintf(sprsht,"by method\t\t\t\t");
	for (meth=0;meth<actualpred-1;meth++)
		fprintf(sprsht,"\t\t\t\t");
	fprintf(sprsht,"total\n");
	for (seq=0;seq<actualnum;seq++)
		fprintf(sprsht,"%s\t\t\t\t",seqname[seq]);
	for (meth=0;meth<actualpred;meth++)
		fprintf(sprsht,"%s\t\t\t\t",predmeth[meth]);
	fprintf(sprsht,"\n");
	for (seq=0;seq<actualnum+actualpred+1;seq++)
		fprintf(sprsht,"H\tE\tC\t\t");
	fprintf(sprsht,"\n");
	for (pos=0;pos<actualsize;pos++)
	{
		for (seq=0;seq<actualnum;seq++)
			fprintf(sprsht,"%d\t%d\t%d\t\t",seqsum[seq].helix[pos],seqsum[seq].sheet[pos],seqsum[seq].coil[pos]);
		for (meth=0;meth<actualpred;meth++)
			fprintf(sprsht,"%d\t%d\t%d\t\t",methsum[meth].helix[pos],methsum[meth].sheet[pos],methsum[meth].coil[pos]);
		fprintf(sprsht,"%d\t%d\t%d",totsum.helix[pos],totsum.sheet[pos],totsum.coil[pos]);
		fprintf(sprsht,"\n");
	}

	for (pos=0;pos<actualsize;pos++)
		fprintf(sprsht,"%d\n",solvat.prob[pos]);
	
	fclose(sprsht);
}

