# nAChR2ndStruct

Those are small C programs I wrote for the paper:

Le Novère N., Corringer P.J. and Changeux J.P. Improved secondary structure predictions for a nicotinic receptor subunit.Incorporation of solvent accessibility and experimental data into a 2D representation. _Biophysical Journal_ (1999), 76: 2329-2345.
