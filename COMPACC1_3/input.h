/***************************************************************************
 *                                 Compacc v1.3                            *
 *                         Nicolas Le Nov�re 1998-2000                     *
 ***************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re 
      R�cepteurs et cognition, Institut Pasteur, 25 rue du Dr ROUX
      75724 Paris c�dex, France. lenov@pasteur.fr
*/

/***********************************************************
 *                         input.h                         *
 ***********************************************************/

/*>>>>>>>>>>>>>>>Preprocessor informations<<<<<<<<<<<<<<<<<*/

#include<stdio.h>
#include<stdlib.h>
#define SEQUENCES 50
#define SIZESEQ 1000
#define PREDICTIONS 10
/*PREDICTIONS contains also the crystal structure*/
#define NAME 10
/*#define DEBUG*/

/*>>>>>>>>>>>>>>>>>>Variables definitions<<<<<<<<<<<<<<<<<<*/

char seqlabel[SEQUENCES][NAME+1]; /*denomination of chains*/
char methlabel[PREDICTIONS+1][NAME+1]; /*denomination of methods*/
char data[SEQUENCES][PREDICTIONS+2][SIZESEQ+1]; /*Contains input data,
                  '+1' denoted the presence of the sequence itself and
                  of the consensus*/
int actualpred=0;  /*actual number of predictions*/
int actualchain=0; /*actual number of chain*/
int actualsize[SEQUENCES];  /*Length of the chains*/
extern int verbose; /*Specify if verbose mode is ON (==0) or OFF(!=0)*/

/*>>>>>>>>>>>>>>>>>>Functions declarations<<<<<<<<<<<<<<<<<*/

void ReadInput(char *input); /*Read the input file*/
