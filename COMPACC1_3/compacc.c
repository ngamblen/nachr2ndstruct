/***************************************************************************
 *                                 Compacc v1.3                            *
 *                         Nicolas Le Nov�re 1998-2000                     *
 ***************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re 
      R�cepteurs et cognition, Institut Pasteur, 25 rue du Dr ROUX
      75724 Paris c�dex, France. lenov@pasteur.fr
*/

/***********************************************************
 *                       compacc.c                         *
 ***********************************************************/

#include"compacc.h"

/***********************************************************
 *                       function main                     *
 ***********************************************************/

int main(int argc, char *argv[])
{
	int count;                        /*loop counter*/

	fprintf(stdout,"***************************************************************************\n");
	fprintf(stdout,"*                                 CompAcc v1.3                            *\n");
	fprintf(stdout,"*                           Nicolas Le Nov�re 1998-2001                   *\n");
	fprintf(stdout,"***************************************************************************\n");

#ifdef DEBUG
	fprintf(stdout,"read the arguments\n");
#endif /*DEBUG*/

	for (count=1;count<argc;count++)
	{	
		switch (argv[count][1])
		{
		case 'I':
			if (argv[count][2]=='\0') /*empty option*/
			{
				fprintf(stderr,"Bad option %s, no input filename specified\n", argv[count]);
				usage();
				exit(1);
			}
			if (argv[count][2]!='\0') /*valid option*/
				in_ptr=&argv[count][2];
			break;
		case 'O':
			if (argv[count][2]=='\0') /*empty option*/
			{
				fprintf(stderr,"No output filename specified  %s\n",argv[count]);
				usage();
				exit(1);
			}
			if (argv[count][2]!='\0') /*valid option*/
				out_ptr=&argv[count][2];
                        break;
		case 'V':
			verbose=1;		  /*turn on the verbose mode*/
			break;
		default:
			fprintf(stderr,"Bad option %s\n",argv[count]);
			usage();
			exit(1);
			break;
		}
	}

       	ReadInput(in_ptr);
       	Consensus();
	Q3chain();
	DetAcc();
	WriteResults(out_ptr);

	return 1;
}

/***********************************************************
 *             Generates output file                       *
 ***********************************************************/

void WriteResults(char *output)
{
   int seq,meth;         /*loop counter*/
   FILE *outfile;        /*ptr on output file*/

   /*Creation of the output file*/

        outfile=fopen(output,"w");
	if (outfile==NULL)
	{
	  fprintf(stderr,"Unable to open-create output file\n");
	  exit(1);
	}

	/*Prints the header*/

#ifdef DEBUG
           fprintf(stdout,"Prints the header\n");
#endif /*DEBUG*/

	fprintf(outfile,"***************************************************************************\n");
	fprintf(outfile,"*                                 CompAcc v1.3                            *\n");
	fprintf(outfile,"*                           Nicolas Le Nov�re 1998-2001                   *\n");
	fprintf(outfile,"***************************************************************************\n");

	/*Prints the input predictions and the computed consensus*/  

#ifdef DEBUG      
	  fprintf(stdout,"Prints the input predictions and the computed consensus\n");
#endif /*DEBUG*/

        fprintf(outfile,"Number of predictions: %d\n",actualpred-1);
        fprintf(outfile,"Number of chains: %d\n",actualchain);

	fprintf(outfile,"****************************************\n");
	fprintf(outfile,"* Predictions and prediction consensus *\n");
	fprintf(outfile,"****************************************\n");
	fprintf(outfile,"\n");
        for (seq=0;seq<actualchain;seq++)
        {
               fprintf(outfile,"%s:\t %s \n", seqlabel[seq], data[seq][0]);
               for (meth=0;meth<actualpred;meth++)
                      fprintf(outfile,"%s:\t %s\n",methlabel[meth], data[seq][meth+1]);
	       fprintf(outfile,"\n");
	       /*Print the consensus of all methods for this chain*/
      	       fprintf(outfile,"%s:\t %s\n",methlabel[actualpred],data[seq][actualpred+1]);
	       fprintf(outfile,"\n");
	}

	/*Prints the Q3 accuracies*/
#ifdef DEBUG      
	fprintf(stdout,"Prints the Q3 accuracies\n");
#endif /*DEBUG*/	


	fprintf(outfile,"***************************\n");
	fprintf(outfile,"* Q3 accuracies per chain *\n");
	fprintf(outfile,"***************************\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"     Palpha+Pbeta+Pcoil\n");
	fprintf(outfile,"Q3 = ------------------\n");
	fprintf(outfile,"        chain length   \n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Where pi is the number of correctly predicted residues in the state i\n");
	fprintf(outfile,"\n");

	fprintf(outfile,"\t");
	for(meth=1;meth<actualpred+1;meth++)
	  fprintf(outfile,"%s\t",methlabel[meth]);
	fprintf(outfile,"\n");
        for (seq=0;seq<actualchain;seq++){
               fprintf(outfile,"%s:\t", seqlabel[seq]);
	       for (meth=0;meth<actualpred;meth++)
		 fprintf(outfile,"%3.2f\t",accuracy[seq][meth]);
	       fprintf(outfile,"\n");
	}

	fprintf(outfile,"\n");
	fprintf(outfile,"Chain\t");
	for (meth=0;meth<actualpred;meth++)
	  fprintf(outfile,"%3.2f\t",perchain[meth].mean);	
        fprintf(outfile,"\n");
	fprintf(outfile,"Stdev\t");
	for (meth=0;meth<actualpred;meth++)
	  fprintf(outfile,"%3.2f\t",perchain[meth].stdev);	

        fprintf(outfile,"\n\n");

	fprintf(outfile,"********************\n");
	fprintf(outfile,"* Actual structure *\n");
	fprintf(outfile,"********************\n");	
	fprintf(outfile,"\n");
	fprintf(outfile,"total number of residus:\t%d\n", numberalpha+numberbeta+numbercoil);
	fprintf(outfile,"total observed alpha residus:\t%d\n", numberalpha);
	fprintf(outfile,"total observed beta residus:\t%d\n", numberbeta);
	fprintf(outfile,"total observed coil residus:\t%d\n", numbercoil);

	fprintf(outfile,"\n");
	fprintf(outfile,"**********************************************\n");
	fprintf(outfile,"* Global accuracie of the prediction methods *\n");
	fprintf(outfile,"**********************************************\n");	
	fprintf(outfile,"\n");
	fprintf(outfile,"         Pi \n");
	fprintf(outfile,"Qiobs = ----\n");
	fprintf(outfile,"         Ni \n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Where pi is the number of correctly predicted residues in the state i\n");
	fprintf(outfile,"and Ni is the number of residues actually in the state i\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"          Pi \n");
	fprintf(outfile,"Qipred = ----\n");
	fprintf(outfile,"          Ti \n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Where pi is the number of correctly predicted residues in the state i\n");
	fprintf(outfile,"and Ti is the total number of residues predicted in the state i\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Q3obs has the same definition as above, but on the total dataset\n");
	fprintf(outfile,"\n");	
        fprintf(outfile,"Matthews correlation coefficients:\n");
	fprintf(outfile,"                PiNi-UiOi           \n");
	fprintf(outfile,"Ci = -------------------------------\n");
	fprintf(outfile,"       /----------------------------\n");
	fprintf(outfile,"     \\/ (Pi+Oi)(Pi+Ui)(Ni+Oi)(Ni+Ui)\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Where Pi is the number of correctly predicted residues in the state i\n");
	fprintf(outfile,"      Ni is the number of residues correctly non-predicted in the state i\n");
	fprintf(outfile,"      Oi is the number of residues over-predicted in the state i\n");
	fprintf(outfile,"      Ui is the number of residues under-predicted in the state i\n");
	fprintf(outfile,"\n");
	fprintf(outfile,"Q3obs has the same definition as above, but on the total dataset\n");
	fprintf(outfile,"\n");	
	for(meth=1;meth<actualpred+1;meth++){
	  fprintf(outfile,"%s\n",methlabel[meth]);
	  fprintf(outfile,"total predicted alpha residus:\t%d\n", numpredalpha[meth-1]);
	  fprintf(outfile,"total predicted beta residus:\t%d\n", numpredbeta[meth-1]);
	  fprintf(outfile,"total predicted coil residus:\t%d\n", numpredcoil[meth-1]);

	  fprintf(outfile,"Qalphaobs:\t%5.2f\n",Qalpha[meth-1]);
	  fprintf(outfile,"Qbetaobs:\t%5.2f\n",Qbeta[meth-1]);
	  fprintf(outfile,"Qcoilobs:\t%5.2f\n",Qcoil[meth-1]);

	  fprintf(outfile,"Q3obs:\t%5.2f\n",Q3obs[meth-1]);

	  fprintf(outfile,"Qalphapred:\t%5.2f\n",Qpredalpha[meth-1]);
	  fprintf(outfile,"Qbetapred:\t%5.2f\n",Qpredbeta[meth-1]);
	  fprintf(outfile,"Qcoilpred:\t%5.2f\n",Qpredcoil[meth-1]);

	  
	  fprintf(outfile,"Calpha:\t%5.2f\n",Calpha[meth-1]);
	  fprintf(outfile,"Cbeta:\t%5.2f\n",Cbeta[meth-1]);
	  fprintf(outfile,"Ccoil:\t%5.2f\n",Ccoil[meth-1]);
	}

        fflush(outfile);
	fclose(outfile);
}

void usage(void)
{
	fprintf(stderr,"Usage is compacc -I[input.txt] -O[output.txt] -V\n");
	fprintf(stderr,"options:\n");
	fprintf(stderr,"-I\tname of the file containing the predictions\n");
 	fprintf(stderr,"\tdefault is input.txt\n");
	fprintf(stderr,"-O\tname of the result file\n");
	fprintf(stderr,"\tdefault is output.txt\n");
	fprintf(stderr,"-V\tturn on the verbose output\n");
    fprintf(stderr,"\tdefault is OFF\n");
}
