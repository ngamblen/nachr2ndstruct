/***************************************************************************
 *                                 Compacc v1.3                            *
 *                         Nicolas Le Nov�re 1998-2000                     *
 ***************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re 
      R�cepteurs et cognition, Institut Pasteur, 25 rue du Dr ROUX
      75724 Paris c�dex, France. lenov@pasteur.fr
*/

/***********************************************************
 *                         calcul.c                         *
 ***********************************************************/

#include"calcul.h"
#include"math.h"

/***************************************************************
 * Computes the consensus of all predictions for each sequence *
 ***************************************************************/

void Consensus(void)
{
    int seq=0,pos=0,meth=0;                       /*loop counters*/

        /*-----------------------------*
         | initialisation of variables |
         *-----------------------------*/
#ifdef DEBUG
      fprintf(stdout, "Consensus: initialisation of variables\n");
#endif /*DEBUG*/

    for (seq=0;seq<actualchain;seq++)
        for (pos=0;pos<actualsize[seq];pos++)
        {
            total[seq].helix[pos]=0;
            total[seq].sheet[pos]=0;
            total[seq].coil[pos]=0;
        }

        /*-----------------------------------------------------*
         | processed data including all methods for each chain |
         *-----------------------------------------------------*/

#ifdef DEBUG
      fprintf(stdout,"Consensus: consensus computation\n");
#endif /*DEBUG*/

    for (seq=0;seq<actualchain;seq++)
    {
        for (pos=0;pos<actualsize[seq];pos++)
        {
            for (meth=2;meth<actualpred+1;meth++)  /*meth=1 is the crystal*/
                switch (data[seq][meth][pos])
                {
                    case 'H':
                        total[seq].helix[pos]++;
                        break;
                    case 'E':
                        total[seq].sheet[pos]++;
                        break;
                    case 'C':
                        total[seq].coil[pos]++;
                        break;
                    default:
                        fprintf(stderr,"Illegal structure character prediction: %c",data[seq][meth][pos]);
                        fprintf(stderr,"sequence: %s, method: %s, position: %d, character: %c\n",seqlabel[seq],methlabel[meth-2],pos,data[seq][meth][pos]);
                        exit(1);
                        break;
                }
            data[seq][actualpred+1][pos]=Majority(total[seq].helix[pos],total[seq].sheet[pos],total[seq].coil[pos]);
        }
        if (verbose)
        {
           fprintf(stdout,"Consensus: consensus for %s:\n",seqlabel[seq]);
           fprintf(stdout,"Consensus: %s\n",data[seq][actualpred+1]);
        }
     } 
}

/******************************************************
 * compute the majority structure at a given position *
 ******************************************************/

char Majority(int helix, int sheet, int coil)
{
        char state='C';
        int  max=0;

        if(coil>=max)
        {
                max=coil;
                state='C';
        }
        if(helix>=max)
        {
                max=helix;
                state='H';
        }
        if(sheet>=max)
        {
                max=sheet;
                state='E';
        }
        return(state);
}

/***********************************
 * Computes the Q3 of a prediction *
 ***********************************/

void Q3chain(void)
{
  int seq=0,meth=0,pos=0;    /*loop counter*/
  int ident=0;         /*number of weel predicted residues*/
  double sumacc=0.0;   /*accuracy weigted by the length*/
  double value[SEQUENCES];   /*temporary array for the descriptive statistics*/

  /*initialisation of variables*/
#ifdef DEBUG
    fprintf(stdout,"Q3 : initialisation of variables\n");
#endif /*DEBUG*/

  for(seq=0;seq<actualchain;seq++)
    value[seq]=0.0;
  for(meth=0;meth<actualpred+1;meth++){
    perchain[meth].sample=0;
    perchain[meth].mean=0.0;      
    perchain[meth].stdev=0.0;     
    perchain[meth].sem=0.0;
  }

  /*--------------------------------------*
   | Computation of Q3 accuracy per chain |
   *--------------------------------------*/

#ifdef DEBUG 
    fprintf(stdout,"Q3 : Computation of predictions Q3\n");
#endif /*DEBUG*/

  for (seq=0;seq<actualchain;seq++){
    /*meth=2 in the following because 0 is the sequence and 1 is the crystal*/
    for (meth=2;meth<actualpred+2;meth++){
      ident=0;
      for (pos=0;pos<actualsize[seq];pos++){
	if (data[seq][meth][pos]==data[seq][1][pos])
	  ident++;
      }
      accuracy[seq][meth-2]=(double)ident/(double)actualsize[seq]*100.0;
      if(verbose)
	fprintf(stdout,"%s %s Q3: %3.2f\n",seqlabel[seq],methlabel[meth-1],accuracy[seq][meth-2]);
    }
  }
  if (verbose)
    fflush(stdout);
  for(meth=0;meth<actualpred;meth++){
    for(seq=0;seq<actualchain;seq++)
      value[seq]=accuracy[seq][meth];
   /*Be careful, here seq is incremented at the end of the loop. Then
to add a value do not use seq+1 but rather seq*/
    value[seq]=-1;
    perchain[meth]=DesStat(value);
  }
}

/***********************************
 * Detailed Accuracies: Q3, Qa, Qb *
 ***********************************/

void DetAcc(void)
{
  int seq=0,meth=0,pos=0;    /*loop counter*/
  long predalpha=0;     
  long nonpredalpha=0;  
  long underalpha=0;    
  long overalpha=0;     
  long predbeta=0;
  long nonpredbeta=0;
  long underbeta=0;
  long overbeta=0;
  long predcoil=0;
  long nonpredcoil=0;
  long undercoil=0;
  long overcoil=0;

#ifdef DEBUG 
    fprintf(stdout,"DetAcc : Initialisation of variables\n");
    fflush(stdout);
#endif /*DEBUG*/

  /*Initialisation of variables*/

  for (meth=0;meth<actualpred+1;meth++){
    numpredalpha[meth]=0;
    numpredbeta[meth]=0;
    numpredcoil[meth]=0;

    Qalpha[meth]=0.0;
    Qpredalpha[meth]=0.0;
    Qbeta[meth]=0.0;
    Qpredbeta[meth]=0.0;
    Qcoil[meth]=0.0;
    Qpredcoil[meth]=0.0;
    Q3obs[meth]=0.0;
    Calpha[meth]=0.0;
    Cbeta[meth]=0.0;
    Ccoil[meth]=0.0;
  }

#ifdef DEBUG 
      fprintf(stdout,"DetAcc : Beginning the loop on methods\n");
      fflush(stdout);
#endif /*DEBUG*/
    /*Beginning the loop on methods*/

  for (meth=2;meth<actualpred+2;meth++){ /*meth=2 to avoid sequence (meth=0) and cristal (meth=1)*/
    predalpha=0;     
    nonpredalpha=0;  
    underalpha=0;    
    overalpha=0;     
    predbeta=0;
    nonpredbeta=0;
    underbeta=0;
    overbeta=0;
    predcoil=0;
    nonpredcoil=0;
    undercoil=0;
    overcoil=0;
    numberalpha=0;
    numberbeta=0;
    numbercoil=0;

#ifdef DEBUG 
      fprintf(stdout,"DetAcc : Beginning the loop on chains\n");
#endif /*DEBUG*/
    /*Beginning the loop on chains*/

    for (seq=0;seq<actualchain;seq++){
      for (pos=0;pos<actualsize[seq];pos++){
	switch(data[seq][1][pos]){ 
	case 'H':
	  numberalpha++;
	  switch(data[seq][meth][pos]){
	  case 'H':
	    numpredalpha[meth-2]++;
	    predalpha++;
	    nonpredbeta++;
	    nonpredcoil++;
	    break;
	  case 'E':
	    numpredbeta[meth-2]++;
	    underalpha++;
	    overbeta++;
	    nonpredcoil++;
	    break;
	  case 'C':
	    numpredcoil[meth-2]++;
	    underalpha++;
	    nonpredbeta++;
	    overcoil++;
	    break;
	  default:
	    fprintf(stderr,"illegal residue: %c, at position %d in prediction %s of the sequence %s\n",data[seq][meth][pos],pos,methlabel[meth-1],seqlabel[seq]);
                                          /*meth-1 because meth=0 is the crystal in methlabel contrary to data where the sequence is at 0 and the crystal at 1)*/
	    exit(1);
	    break;
	  }
	  break;
	case 'E':
	  numberbeta++;
	  switch(data[seq][meth][pos]){
	  case 'H':
	    numpredalpha[meth-2]++;
	    overalpha++;
	    underbeta++;
	    nonpredcoil++;
	    break;
	  case 'E':
	    numpredbeta[meth-2]++;
	    nonpredalpha++;
	    predbeta++;
	    nonpredcoil++;
	    break;
	  case 'C':
	    numpredcoil[meth-2]++;
	    nonpredalpha++;
	    underbeta++;
	    overcoil++;
	    break;
	  default:
	    fprintf(stderr,"illegal residue: %c, at position %d in prediction %s of the sequence %s\n",data[seq][meth][pos],pos,methlabel[meth-1],seqlabel[seq]);
                                          /*meth-1 because meth=0 is the crystal in methlabel contrary to data where the sequence is at 0 and the crystal at 1)*/
	    exit(1);
	    break;
	  }
	  break;
	case 'C':
	  numbercoil++;
	  switch(data[seq][meth][pos]){
	  case 'H':
	    numpredalpha[meth-2]++;
	    overalpha++;
	    nonpredbeta++;
	    undercoil++;
	    break;
	  case 'E':
	    numpredbeta[meth-2]++;
	    nonpredalpha++;
	    overbeta++;
	    undercoil++;
	    break;
	  case 'C':
	    numpredcoil[meth-2]++;
	    nonpredalpha++;
	    nonpredbeta++;
	    predcoil++;
	    break;
	  default:
	    fprintf(stderr,"illegal residue: %c, at position %d in prediction %s of the sequence %s\n",data[seq][meth][pos],pos,methlabel[meth-1],seqlabel[seq]);
                                          /*meth-1 because meth=0 is the crystal in methlabel contrary to data where the sequence is at 0 and the crystal at 1)*/
	    exit(1);
	    break;
	  }
	  break;
	default:
	  fprintf(stderr,"illegal residue: %c, at position %d in crystal of the sequence %s\n",data[seq][1][pos],pos,seqlabel[seq]); 
	  exit(1);
	  break;
	}
      }
    }

    /*------------*
     | Accuracies |
     *------------*/
    
    if (numberalpha>0)
      Qalpha[meth-2]=(double)(predalpha)/(double)(numberalpha)*100.0;
    else
      Qalpha[meth-2]=-99.99;

    if (numpredalpha[meth-2]>0) 
      Qpredalpha[meth-2]=(double)(predalpha)/(double)(numpredalpha[meth-2])*100.0;
    else
      Qpredalpha[meth-2]=-99.99;

    if (numberbeta>0)    
      Qbeta[meth-2]=(double)(predbeta)/(double)(numberbeta)*100.0;
    else
      Qbeta[meth-2]=-99.99;

    if (numpredbeta[meth-2]>0) 
      Qpredbeta[meth-2]=(double)(predbeta)/(double)(numpredbeta[meth-2])*100.0;
    else
      Qpredbeta[meth-2]=-99.99;

    if (numbercoil>0)    
      Qcoil[meth-2]=(double)(predcoil)/(double)(numbercoil)*100.0;
    else
      Qcoil[meth-2]=-99.99;

    if (numpredcoil[meth-2]>0) 
      Qpredcoil[meth-2]=(double)(predcoil)/(double)(numpredcoil[meth-2])*100.0;
    else
      Qpredcoil[meth-2]=-99.99;

    if (numberalpha+numberbeta+numbercoil>0)
      Q3obs[meth-2]=(predalpha+predbeta+predcoil)/(double)(numberalpha+numberbeta+numbercoil)*100.0;
    else
      Q3obs[meth-2]=-99.99;

    if(verbose){
      fprintf(stdout,"Accuracies\n");
      fprintf(stdout,"DetAcc: methode %d\n",meth-2);
      fprintf(stdout,"DetAcc: numberalpha: %d\n",numberalpha);
      fprintf(stdout,"DetAcc: Qalpha: %5.2f\n", Qalpha[meth-2]);
      fprintf(stdout,"DetAcc: numpredalpha: %d\n",numpredalpha[meth-2]);
      fprintf(stdout,"DetAcc: Qpredalpha: %5.2f\n", Qpredalpha[meth-2]);
      fprintf(stdout,"DetAcc: numberbeta: %d\n",numberbeta);
      fprintf(stdout,"DetAcc: Qbeta: %5.2f\n", Qbeta[meth-2]);
      fprintf(stdout,"DetAcc: numpredbeta: %d\n",numpredbeta[meth-2]);
      fprintf(stdout,"DetAcc: Qpredbeta: %5.2f\n", Qpredbeta[meth-2]);
      fprintf(stdout,"DetAcc: numbercoil: %d\n",numbercoil);
      fprintf(stdout,"DetAcc: Qcoil: %5.2f\n", Qcoil[meth-2]);
      fprintf(stdout,"DetAcc: numpredcoil: %d\n",numpredcoil[meth-2]);
      fprintf(stdout,"DetAcc: Qpredcoil: %5.2f\n", Qpredcoil[meth-2]);
    }
    /*--------------------------------------*
     | Computation of Matthews coefficients |
     *--------------------------------------*/

    if(verbose){
      fprintf(stdout,"Computation of Matthews coefficients\n");      
      fprintf(stdout,"methode %d\n",meth-2);
      fprintf(stdout,"predalpha: %d\n", predalpha );
      fprintf(stdout,"nonpredalpha: %d\n", nonpredalpha );
      fprintf(stdout,"underalpha: %d\n", underalpha );
      fprintf(stdout,"overalpha: %d\n", overalpha );
      fprintf(stdout,"predbeta: %d\n", predbeta);
      fprintf(stdout,"nonpredbeta: %d\n", nonpredbeta);
      fprintf(stdout,"underbeta: %d\n", underbeta);
      fprintf(stdout,"overbeta: %d\n", overbeta);
      fprintf(stdout,"predcoil: %d\n", predcoil);
      fprintf(stdout,"nonpredcoil: %d\n", nonpredcoil);
      fprintf(stdout,"overcoil: %d\n", overcoil);
      fprintf(stdout,"undercoil: %d\n", undercoil);
      fflush(stdout);
    }
 
    if ((predalpha+underalpha)*(predalpha+overalpha)*(nonpredalpha+underalpha)*(nonpredalpha+overalpha)==0)
      Calpha[meth-2]=-99.99;
    else
      Calpha[meth-2]=(double)((predalpha * nonpredalpha)-(underalpha * overalpha))/sqrt((double)(predalpha+underalpha)*(predalpha+overalpha)*(nonpredalpha+underalpha)*(nonpredalpha+overalpha));

    if((predbeta+underbeta)*(predbeta+overbeta)*(nonpredbeta+underbeta)*(nonpredbeta+overbeta)==0)
      Cbeta[meth-2]=-99.99;
    else
      Cbeta[meth-2]=(double)((predbeta * nonpredbeta)-(underbeta * overbeta))/sqrt((double)(predbeta+underbeta)*(predbeta+overbeta)*(nonpredbeta+underbeta)*(nonpredbeta+overbeta));

    if((predcoil+undercoil)*(predcoil+overcoil)*(nonpredcoil+undercoil)*(nonpredcoil+overcoil)==0)
      Ccoil[meth-2]=-99.99;
    else
      Ccoil[meth-2]=(double)((predcoil * nonpredcoil)-(undercoil * overcoil))/sqrt((double)(predcoil+undercoil)*(predcoil+overcoil)*(nonpredcoil+undercoil)*(nonpredcoil+overcoil));
  }
}


/**************************
 * Descriptive statistics *
 **************************/

struct stat DesStat (double *value_ptr)
{
        int number=0;           /*population*/
        double sum=0.0;         /*sum of values*/
        double sumsq=0.0;       /*sum of squared values*/
        double sumdist=0.0;     /*sum of dist to the mean*/
        double *temp_ptr;
        struct stat statvalue;

        temp_ptr=value_ptr;
        while (*temp_ptr!=-1)
        {
                if (*temp_ptr!=0.0)
                        number++;
                sum+=*temp_ptr;
                sumsq+=(*temp_ptr)*(*temp_ptr);

                temp_ptr++;
        }
        statvalue.sample=number;
        statvalue.mean=sum/(double)number;
        temp_ptr=value_ptr;
        while (*temp_ptr>=0)
        {
                sumdist+=((*temp_ptr)-statvalue.mean)*((*temp_ptr)-statvalue.mean);
                temp_ptr++;
        }
        if (sumdist>=0)
                statvalue.sem=sqrt(sumdist/((double)number*(double)number));
        statvalue.stdev = sqrt( ((double)number*sumsq-sum*sum)/((double)number*(double)number) );
        return (statvalue);
}


