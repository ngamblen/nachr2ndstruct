/***************************************************************************
 *                                 Compacc v1.3                            *
 *                         Nicolas Le Nov�re 1998-2000                     *
 ***************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re 
      R�cepteurs et cognition, Institut Pasteur, 25 rue du Dr ROUX
      75724 Paris c�dex, France. lenov@pasteur.fr
*/

/***********************************************************
 *                         input.c                         *
 ***********************************************************/

#include"input.h"

/***********************
 * Read the input file *
 ***********************/

void ReadInput(char *input)
{
        int  seq=0,meth=0,pos=0;      /*counters*/
        char trash;             /*wastebasket*/
        char line[SIZESEQ+6];   /*scanning line*/
        FILE *inputpred;        /*ptr on prediction file*/

	/*initialisation of the variables*/

#ifdef DEBUG
           fprintf(stdout,"ReadInput: initialisation of the array variables\n");
#endif /*DEBUG*/

        memset(data,'\0',SEQUENCES*(PREDICTIONS+2)*(SIZESEQ+1));
        memset(seqlabel,'\0',SEQUENCES*(NAME+1));
        memset(methlabel,'\0',(PREDICTIONS+1)*(NAME+1));
        memset(actualsize,0,SEQUENCES);

        /*Opening the prediction file*/
#ifdef DEBUG
           fprintf(stdout,"ReadInput: Opening the prediction file\n");
#endif /*DEBUG*/

        inputpred=fopen(input,"r");
       	if (inputpred==NULL)
	{
	  fprintf(stderr,"Unable to open prediction file\n");
	  exit(1);
	}
        /*Reading the prediction file*/

#ifdef DEBUG
           fprintf(stdout,"ReadInput: Reading the prediction file\n");        
#endif /*DEBUG*/
        
	 while(1)
	 {
            if (feof(inputpred))
                  break;	   
                fgets(line,sizeof(line),inputpred);
                switch (line[0])
                {
                        case '#':       /*sequence name on this line*/
                                sscanf(line,"%c %s %s",&trash,seqlabel[seq],data[seq][0]);
#ifdef DEBUG
				  fprintf(stdout,"ReadInput: Sequence %d: %s\n", seq, seqlabel[seq]);
				  fprintf(stdout,"ReadInput: %s\n", data[seq][0]);
#endif /*DEBUG*/
                                meth=0;
                                seq++;
                                if (seq>actualchain)
				  {
                                        actualchain=seq;
#ifdef DEBUG
					  fprintf(stdout,"ReadInput: actualchain: %d\n",actualchain);
#endif /*DEBUG*/
				  }
                                break;
                        case '$':       /*method name on this line*/
                                sscanf(line,"%c %s %s",&trash,methlabel[meth],data[seq-1][meth+1]);
#ifdef DEBUG
				  fprintf(stdout,"ReadInput: Method %d: %s\n", meth, methlabel[meth]);
				  fprintf(stdout,"ReadInput: %s\n", data[seq-1][meth+1]);
#endif /*DEBUG*/
				        /*seq-1 because of previous incrementation*/
                                        /*meth+1 because 0 is the sequence*/
                                if (meth+1>actualpred)
                                        actualpred=meth+1;
                                meth++;
                                break;
		        case '\n':
			    break;
                        default:
                                fprintf(stderr,"Illegal character found at first line position: %c",line[0]);
                                exit(1);
                                break;
                }
        }
#ifdef DEBUG
          fprintf(stdout,"ReadInput: End of prediction reading\n");
#endif /*DEBUG*/
	if (verbose)
        {
	  fprintf(stdout,"ReadInput: actualpred: %d\n",actualpred);
	  fprintf(stdout,"ReadInput: actualchain: %d\n",actualchain);
        }
        fclose(inputpred);

        for (seq=0;seq<actualchain;seq++)
        {
	    pos=0;
            while(data[seq][0][pos]!='\0')
                 actualsize[seq]++,pos++;
	    if (verbose)
	      fprintf(stdout,"ReadInput: Size of sequence %s: %d\n",seqlabel[seq],actualsize[seq]);
	}
	strcpy(methlabel[actualpred],"CONS");
}
