/***************************************************************************
 *                                 Compacc v1.3                            *
 *                         Nicolas Le Nov�re 1998-2000                     *
 ***************************************************************************/

/*    This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

      Nicolas Le Nov�re 
      R�cepteurs et cognition, Institut Pasteur, 25 rue du Dr ROUX
      75724 Paris c�dex, France. lenov@pasteur.fr
*/

/***********************************************************
 *                         calcul.h                         *
 ***********************************************************/

/*>>>>>>>>>>>>>>>Preprocessor informations<<<<<<<<<<<<<<<<<*/

#include<stdio.h>
#include<stdlib.h>
/*#define DEBUG*/
#define SEQUENCES 50
#define SIZESEQ 1000
#define PREDICTIONS 10
/*Do not forget: PREDICTIONS is 1 more than actual predictions*/
#define NAME 10

/*>>>>>>>>>>>>>>>>>>Variables definitions<<<<<<<<<<<<<<<<<<*/

struct consensus              /*processed data for 1 element*/
{
        int helix[SIZESEQ];   /*Number of predicted helix*/
        int sheet[SIZESEQ];   /*Number of predicted sheet*/
        int coil[SIZESEQ];    /*Number of predicted coil*/
} total[SEQUENCES];

double accuracy[SEQUENCES][PREDICTIONS]; /*record the Q3 accuracies
BE CAREFUL, if n predicitions, the n+1 line contains the accuracy of consensus*/

int numberalpha=0;     /*total number of alpha residues in test set*/
int numberbeta=0;      /*total number of beta residues in test set*/
int numbercoil=0;      /*total number of coil residues in test set*/
int numpredalpha[PREDICTIONS];    /*total number of predicted residues in alpha*/
int numpredbeta[PREDICTIONS];     /*total number of predicted residues in beta*/
int numpredcoil[PREDICTIONS];     /*total number of predicted residues in coil*/

double Qalpha[PREDICTIONS];         /*observed accuracy for alpha*/
double Qpredalpha[PREDICTIONS];   /*predicteded accuracy for alpha*/
double Qbeta[PREDICTIONS];        /*observed accuracy for beta*/
double Qpredbeta[PREDICTIONS];    /*predicted accuracy for beta*/
double Qcoil[PREDICTIONS];        /*observed accuracy for coil*/
double Qpredcoil[PREDICTIONS];    /*predicteded accuracy for coil*/
double Q3obs[PREDICTIONS];           /*overall observed accuracy*/
double Calpha[PREDICTIONS];       /*Matthews correlation coefficient for alpha*/
double Cbeta[PREDICTIONS];        /*Matthews correlation coefficient for beta*/
double Ccoil[PREDICTIONS];        /*Matthews correlation coefficient for coil*/

struct stat
{
        int sample;         /*sample size*/
        double mean;        /*mean of an array*/
        double stdev;       /*standard deviation of an array*/
        double sem;         /*standard deviation to the mean*/
} perchain[PREDICTIONS];  /*all the per chain accuracies*/  

extern int actualpred;        /*actual number of predictions*/
extern int actualchain;       /*actual number of chain*/
extern int actualsize[SEQUENCES];  /*Length of the chains*/
extern char data[SEQUENCES][PREDICTIONS+2][SIZESEQ+1]; /*Contains input data,
                  '+1' denotes the presence of the sequence itself*/
extern char seqlabel[SEQUENCES][NAME+1]; /*denomination of chains*/
extern char methlabel[PREDICTIONS+1][NAME+1]; /*denomination of methods*/

extern int  verbose;          /*Precise the verbose mode: 0 if OFF, !=0 if ON*/

/*>>>>>>>>>>>>>>>>>>Functions declarations<<<<<<<<<<<<<<<<<*/

void Consensus(void);         /*Computes the consensus of all 
                             predictions for each sequence*/

char Majority(int helix,int sheet,int coil); /*Computes the majority state at one position*/

void Q3chain(void);                /*Computes the Q3 of a prediction*/
void DetAcc(void);            /*Detailed Accuracies: Q3, Qa, Qb */
struct stat DesStat (double *value_ptr); /*Descriptive statistics*/
